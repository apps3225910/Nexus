//
//  SwiftUIHelper.swift
//  Nexus
//
//  Created by Pascal Hintze on 02.03.2025.
//

import SwiftUI
import Combine

extension NotificationCenter {
    var storeDidChangePublisher: Publishers.ReceiveOn<NotificationCenter.Publisher, DispatchQueue> {
        return publisher(for: .cdcksStoreDidChange).receive(on: DispatchQueue.main)
    }
}
