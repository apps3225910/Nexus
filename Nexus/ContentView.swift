//
//  ContentView.swift
//  Nexus
//
//  Created by Pascal Hintze on 06.04.2024.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("isUserRegistered") private var isUserRegistered = false

    @State private var selectedItem = 1

    var body: some View {
        if isUserRegistered == true {
            TabView(selection: $selectedItem) {
                GroupListView()
                    .tabItem {
                        Label("Groups", systemImage: "person.3.fill")
                    }.tag(2)
                Text("Activities")
                    .tabItem {
                        Label("Activities", systemImage: "doc.badge.clock")
                    }.tag(3)
                ProfileView()
                    .tabItem {
                        Label("Account", systemImage: "person.crop.circle")
                    }.tag(4)
            }
        } else {
            LoginView()
        }
    }
}

#Preview {
    ContentView()
}
