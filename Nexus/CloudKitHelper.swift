//
//  CloudKitHelper.swift
//  Nexus
//
//  Created by Pascal Hintze on 11.06.2024.
//

import Foundation
import CloudKit
import Model

 class CloudKitHelper: ObservableObject {
    @Published var useriCloudId: String?
    @Published var userExists: Bool?

//     func checkIfUserAlreadyExists() async {
//         let userId = try? await CKContainer.default().userRecordID().recordName
//
//         guard let useriCloudId else {
//             
//         }
//     }

    func getUseriCloudId() {
            CKContainer.default().fetchUserRecordID { (recordID, error) in
                DispatchQueue.main.async {
                    if let userID = recordID?.recordName {
                        self.useriCloudId = userID
                        print("The users iCloud ID is \(userID)")

                        self.checkIfUserExists(iCloudId: userID) { exists in
                            self.userExists = exists
                        }
                    } else if let error = error {
                        // Return an empty string or handle the error as needed
                        self.useriCloudId = ""
                        print("An error occurred: \(error.localizedDescription)")
                    }
                }
            }
        }

    // Check if the user already exists in CloudKit public db
    func checkIfUserExists(iCloudId: String, completion: @escaping (Bool) -> Void) {

        guard !iCloudId.isEmpty else {
            print("iCloudId is empty")
            completion(false)
            return
        }

        DispatchQueue.main.async {
            let predicate = NSPredicate(format: "CD_ckID == %@", iCloudId)
            let query = CKQuery(recordType: "CD_User", predicate: predicate)

            let publicDatabase = CKContainer.default().publicCloudDatabase

            publicDatabase.fetch(withQuery: query, inZoneWith: nil, desiredKeys: nil, resultsLimit: 1) { result in
                switch result {
                case .success((let matchResults, _)):
                    completion(!matchResults.isEmpty)
                    self.userExists = true
                    print("User does exist with \(iCloudId)")
                case .failure(let error):
                    print(error.localizedDescription)
                    completion(false)
                    self.userExists = false
                    print("User does not exist with \(iCloudId)")
                }
            }
        }
    }
}
