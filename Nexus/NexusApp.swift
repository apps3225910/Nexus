//
//  NexusApp.swift
//  Nexus
//
//  Created by Pascal Hintze on 06.04.2024.
//

import Model
import SwiftUI
import CoreData

@main
struct NexusApp: App {
    @UIApplicationDelegateAdaptor var appDelegate: AppDelegate

    private let persistenceContainer = PersistenceController.shared.persistentContainer

    @Environment(\.managedObjectContext) private var viewContext

    init() {
        PersistenceController.shared.deleteFlaggedPayments(
            context: PersistenceController.shared.persistentContainer.viewContext
        )

        PersistenceController.shared.deleteFlaggedGroups(
            context: PersistenceController.shared.persistentContainer.viewContext
        )

        PersistenceController.shared.deleteFlaggedSettledPayments(
            context: PersistenceController.shared.persistentContainer.viewContext
        )
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceContainer.viewContext)
        }
    }
}
