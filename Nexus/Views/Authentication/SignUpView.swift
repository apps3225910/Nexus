//
//  SignUpView.swift
//  Nexus
//
//  Created by Pascal Hintze on 30.05.2024.
//

import SwiftUI

struct SignUpView: View {
    @State private var email = ""
    @State private var firstname = ""
    @State private var surname = ""
    @State private var password = ""
    @State private var confirmPassword = ""

    @Environment(\.dismiss) var dismiss

    var body: some View {

        VStack(spacing: 24) {
            InputView(text: $email, title: "Email Address", placeholder: "Enter Email Address")

            InputView(text: $firstname, title: "FirstName", placeholder: "Enter your First Name")

            InputView(text: $surname, title: "SurName", placeholder: "Enter your Sur Name")

            InputView(text: $password, title: "Password", placeholder: "Enter your Password", isSecureField: true)

            InputView(text: $confirmPassword, title: "Confirm Password", placeholder: "Confirm your Password",
                      isSecureField: true)

        }
        .padding(.horizontal)
        .padding(.top, 12)

        Button {
           print("Signed up")
        } label: {
            HStack {
                Text("SIGN UP")
                    .fontWeight(.semibold)
                Image(systemName: "arrow.right")
            }
            .foregroundStyle(.white)
            .frame(width: UIScreen.main.bounds.width - 32, height: 48)
        }
        .background(Color(.systemBlue))
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .padding(.top, 24)

        Spacer()

        Button {
            dismiss()
        } label: {
            HStack(spacing: 3) {
                Text("Don't want to select a name?")
                Text("Skip")
                    .fontWeight(.bold)
            }
            .font(.system(size: 14))
        }
    }
}

// #Preview {
//    SignUpView()
// }
