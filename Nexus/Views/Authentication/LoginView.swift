//
//  LoginView.swift
//  Nexus
//
//  Created by Pascal Hintze on 30.05.2024.
//

import SwiftUI
import Model
import CoreImage.CIFilterBuiltins
import PhotosUI
import CloudKit

struct LoginView: View {
    @Environment(\.managedObjectContext) var viewContext

    @State private var username = ""
    @State private var userItem: PhotosPickerItem?
    @State private var userImage: Data?

    @AppStorage("isUserRegistered") private var isUserRegistered = false

    var body: some View {
        NavigationStack {

            PhotoPicker(userItem: $userItem, userImage: $userImage, text: "Change your Profile Picture")

            VStack(spacing: 24) {
                InputView(text: $username, title: "Username", placeholder: "Enter your Username")
            }
            .padding(.horizontal)
            .padding(.top, 12)

            Button {
                Task {
                    let userICloudId = try? await CKContainer.default().userRecordID().recordName
                    if let userId = userICloudId {
                        await PersistenceController.shared.addUser(username: username,
                                                                   iCloudID: userId,
                                                                   profilepicture: userImage,
                                                                   context: viewContext)
                        try viewContext.save()

                        isUserRegistered = true
                    }
                }
            } label: {
                HStack {
                    Text("SIGN IN")
                        .fontWeight(.semibold)
                    Image(systemName: "arrow.right")
                }
                .foregroundStyle(.white)
                .frame(width: UIScreen.main.bounds.width - 32, height: 48)
            }
            .background(Color(.systemBlue))
            .clipShape(RoundedRectangle(cornerRadius: 10))
            .padding(.top, 24)

            Spacer()
        }
    }
}

#Preview {
    LoginView()
}
