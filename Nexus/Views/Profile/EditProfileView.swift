//
//  EditProfileView.swift
//  Nexus
//
//  Created by Pascal Hintze on 07.06.2024.
//

import SwiftUI
import Model
import CoreImage.CIFilterBuiltins
import PhotosUI

struct EditProfileView: View {
        @State private var username = ""

        @Environment(\.dismiss) var dismiss
        @Environment(\.managedObjectContext) var viewContext

        @State private var showAlert = false

    @State private var userItem: PhotosPickerItem?
    @State private var userImage: Data?

    let user: User

    let context = CIContext()
    let filter = CIFilter.qrCodeGenerator()

    init(user: User) {
        self.user = user
        userImage = user.profilepicture
    }

        var body: some View {
            NavigationStack {
                VStack {
                    Image(uiImage: UIImage(data: user.profilepicture) ?? UIImage())
                        .resizable()
                        .font(.title)
                        .fontWeight(.semibold)
                        .foregroundStyle(.white)
                        .frame(width: 72, height: 72)
                        .background(Color(.systemGray3))
                        .clipShape(Circle())

                    PhotoPicker(userItem: $userItem, userImage: $userImage, text: "Change Profile Picture")

//                    VStack(spacing: 24) {
//                        InputView(text: $username, title: "Username", placeholder: "Enter your Display Name")
//                    }
//                    .padding(.horizontal)
//                    .padding(.top, 12)

                    Button("Cancel", role: .cancel) {
                        dismiss()
                    }
                }
                .padding(.vertical)
                .toolbar {
                    ToolbarItem {
                        EditButton()
                    }
                }
                .alert("Incomplete Name", isPresented: $showAlert) {

                } message: {
                    Text("Please enter a Display Name.")
                }
            }
        }
}

// #Preview {
//     EditProfileView(user.)
// }
