//
//  ProfileView.swift
//  Nexus
//
//  Created by Pascal Hintze on 30.05.2024.
//

import SwiftUI
import Model

struct ProfileView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(value: true)
    ) private var users: FetchedResults<Model.User>

    @State private var currency: String = "USD"

    let currencies = Locale.Currency.isoCurrencies

    var body: some View {
        NavigationStack {
            if let user = users.first {
                List {
                    Section {
                        NavigationLink {
                            EditProfileView(user: user)
                                .navigationBarBackButtonHidden(true)
                        } label: {
                            HStack {
                                Image(uiImage: UIImage(data: user.profilepicture) ?? UIImage())
//                                Image(systemName: "person")
                                    .resizable()
                                    .scaledToFill()
                                    .font(.title)
                                    .fontWeight(.semibold)
                                    .foregroundStyle(.white)
                                    .frame(width: 72, height: 72)
                                    .background(Color(.systemGray3))
                                    .clipShape(Circle())

                                VStack(alignment: .leading, spacing: 4) {
                                    Text(user.username)
                                        .font(.subheadline)
                                        .fontWeight(.semibold)
                                        .padding(.top, 4)

                                    Text("Details")
                                        .font(.footnote)
                                        .foregroundStyle(.gray)
                                }
                            }
                        }
                    } footer: {
                        Text("Your name and photo will be shown to your friends and in groups.")
                    }

                    Section("Settings") {
                        HStack {
                            Picker(selection: $currency) {
                                ForEach(currencies, id: \.self) { currency in
                                    Text(currency.identifier).tag(currency.identifier)
                                }
                            } label: {
                                SettingsRowView(imageName: "dollarsign.circle", title: "Currency",
                                                tintColor: Color(.systemGreen))
                            }
                        }

                        HStack {
                            SettingsRowView(imageName: "bell", title: "Notifications", tintColor: Color(.systemYellow))
                        }
                        HStack {
                            SettingsRowView(imageName: "sun.max.fill", title: "Appearance",
                                            tintColor: Color(.systemBlue))
                        }

                        HStack {
                            SettingsRowView(imageName: "gear", title: "Version", tintColor: Color(.systemGray))

                            Spacer()

                            Text("1.0.1")
                                .font(.subheadline)
                                .foregroundStyle(.gray)
                        }
                    }
                }
            }
        }
    }

    //    func delete(at offsets: IndexSet) {
    //        for index in offsets {
    //            let user = users[index]
    //            viewContext.delete(user)
    //        }
    //        PersistenceController.shared.save()
    //    }
}

#Preview {
    ProfileView()
}
