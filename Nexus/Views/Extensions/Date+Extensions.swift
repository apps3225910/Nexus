//
//  Date+Extensions.swift
//  Nexus
//
//  Created by Pascal Hintze on 17.12.2024.
//

import Foundation

extension Date {
    func formattedMonthAndYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter.string(from: self)
    }
}
