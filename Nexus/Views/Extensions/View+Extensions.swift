//
//  View+Extensions.swift
//  Nexus
//
//  Created by Pascal Hintze on 27.01.2025.
//

import SwiftUI
import Foundation

extension View {
    // Allows to hides the keyboard to prevent an error when the confirmationDialog appears

    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
