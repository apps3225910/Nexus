//
//  GroupPaymentSplitView.swift
//  Nexus
//
//  Created by Pascal Hintze on 22.08.2024.
//

import SwiftUI
import Model

struct GroupPaymentSplitView: View {
    @Binding var amounts: [String: Double]
    @Binding var percentages: [String: Double]

    @State private var totalPercent: Double = 0

    let group: Model.Group
    let paymentSplit: PaymentSplit
    let currency: String

    var body: some View {
        switch paymentSplit {
        case .equally:
            ForEach(Array(group.groupMember), id: \.self) { member in
                HStack {
                    LabeledContent {
                        TextField("Amount", value: Binding(
                            get: {self.amounts[member.friendId] ?? 0},
                            set: { self.amounts[member.friendId] = $0 }),
                                  format: .number)
                        .multilineTextAlignment(.trailing)
                        .disabled(true)
                        Text(currency)
                    } label: {
                        Text(member.friendName)
                    }
                }
            }
        case .percent:
            ForEach(Array(group.groupMember), id: \.self) { member in
                HStack {
                    LabeledContent {
                        TextField("Select Percentage", value: Binding(
                            get: {self.percentages[member.friendId] ?? 0},
                            set: { self.percentages[member.friendId] = $0 }),
                                  format: .number)
                        .multilineTextAlignment(.trailing)
                        Text("%")

//                        TextField("Select Percentage", value: Binding(
//                            get: {self.amounts[member.friendId] ?? 0},
//                            set: { self.amounts[member.friendId] = $0 }),
//                                  format: .number)
//                        .multilineTextAlignment(.trailing)
//                        Text("%")

                    } label: {
                        Text(member.friendName)
                    }
                }
            }

            Section {
                if totalPercent != 100 {
                    Text("\(totalPercent.formatted())% does not equal 100%")
                        .font(.footnote)
                        .foregroundStyle(.gray)
                }
            }
            .onChange(of: percentages) {
                totalPercent = 0
                for member in group.groupMember {
                    totalPercent += percentages[member.friendId] ?? 0
                }
            }

        case .custom:
            ForEach(Array(group.groupMember), id: \.self) { member in
                HStack {
                    LabeledContent {
                        TextField("Amount", value: Binding(
                            get: {self.amounts[member.friendId] ?? 0},
                            set: { self.amounts[member.friendId] = $0 }),
                                  format: .number)
                        .multilineTextAlignment(.trailing)
                        Text(currency)
                    } label: {
                        Text(member.friendName)
                    }
                }
            }
        }
    }
}

#Preview {
    GroupPaymentSplitView(
        amounts: .constant(["1234560": 5, "654321": 5]),
        percentages: .constant(["1234560": 0, "654321": 0]),
        group: .exampleGroup,
        paymentSplit: .equally,
        currency: "USD"
    )
}
