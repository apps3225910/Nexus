//
//  AddGroupPaymentView.swift
//  Nexus
//
//  Created by Pascal Hintze on 25.07.2024.
//

import CloudKit
import CoreData
import Model
import SwiftUI

struct AddGroupPaymentView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) private var viewContext

    let group: Model.Group

    @State private var amounts: [String: Double] = [:]
    @State private var percentages: [String: Double] = [:]

    @State private var paymentCategory: PaymentCategory = .general
    @State private var amount: Double = 0
    @State private var currency: String = "USD"
    @State private var creationDate: Date = .now
    @State private var description: String = ""

    let currencies = Locale.Currency.isoCurrencies

    @State private var totalMemberAmounts: Double = 0
    @State private var totalMemberPercentages: Double = 0

    @State private var paymentSplit: PaymentSplit = .equally

    @State private var showSumAlert: Bool = false
    @State private var showDescriptionAlert: Bool = false
    @State private var showPercentAlert: Bool = false

    @State private var paidBy: GroupMember

    init(group: Model.Group) {
        self.group = group
        self.paidBy = group.groupMember.first!
    }

    enum FocusedField {
        case description
    }

    @FocusState private var focusedField: FocusedField?

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    TextField("Description", text: $description, prompt: Text("Enter a description"))
                        .focused($focusedField, equals: .description)

                    Picker("Type:", selection: $paymentCategory) {
                        ForEach(PaymentCategory.allCases, id: \.self) { type in
                            Label(type.name, systemImage: type.icon)
                                .labelStyle(.iconOnly)
                        }
                    }
                    .labelsHidden()
                }

                LabeledContent {
                    TextField("Amount", value: $amount, format: .number, prompt: Text("Select amount"))
                        .keyboardType(.decimalPad)
                        .multilineTextAlignment(.trailing)
                } label: {
                    Text("Amount")
                }

                Picker("Currency", selection: $currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                DatePicker("Date", selection: $creationDate, displayedComponents: [.date])
                    .pickerStyle(.wheel)

                Section("Paid by") {
                    Picker("Paid by", selection: $paidBy) {
                        ForEach(Array(group.groupMember), id: \.self) { member in
                            Text(member.friendName).tag(member as GroupMember?)
                        }
                    }
                }

                Section("Splitted") {
                    Picker("Splitted", selection: $paymentSplit) {
                        ForEach(PaymentSplit.allCases, id: \.self) { split in
                            Text(split.name)
                        }
                    }
                    .pickerStyle(.segmented)
                }

                GroupPaymentSplitView(amounts: $amounts, percentages: $percentages, group: group,
                                      paymentSplit: paymentSplit, currency: currency)
            }
            .onAppear {
                focusedField = .description
            }
            .defaultFocus($focusedField, .description)

            .alert("Sum is Not Adding Up", isPresented: $showSumAlert) {
                Button("OK") { }
            } message: {
                Text("The splitted sum is not adding up to the total of the payment.")
            }

                .alert("Percentages are Not Adding Up", isPresented: $showPercentAlert) {
                    Button("OK") { }
                } message: {
                    Text("The percentages are not adding up to 100%.")
                }

            .alert("Please Enter a Description", isPresented: $showDescriptionAlert) {
                Button("OK") { }
            }
            .onChange(of: amount) {
                setValues()
            }
            .onChange(of: paymentSplit) {
                setValues()
                print("Amounts \(amounts)")
            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel") {
                        dismiss()
                    }
                }

                ToolbarItem(placement: .confirmationAction) {
                    Button("Save") {
                        calculateTotals()

                        if description.isEmpty {
                            showDescriptionAlert.toggle()
                        } else {
                            // Set the amounts to the percentages
                            if paymentSplit == .percent {
                                for member in group.groupMember {
                                    amounts[member.friendId] = amount *
                                    (Double(percentages[member.friendId] ?? 0) / 100)
                                }
                            }
                            Task {
                                let expenseInfo = ExpenseInfo(
                                    amount: amount,
                                    currency: currency,
                                    date: creationDate,
                                    category: paymentCategory,
                                    description: description,
                                    paidBy: paidBy,
                                    paymentSplit: paymentSplit,
                                    group: group,
                                    amounts: amounts)

                                await PersistenceController.shared.createGroupPayment(
                                    expenseInfo: expenseInfo,
                                    context: viewContext)
                            }
                            dismiss()
                        }
                    }
                }
            }
            .navigationTitle("New Payment")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarBackButtonHidden()
        }
    }

    private func setValues() {
        switch paymentSplit {
        case .equally:
            for member in group.groupMember {
                amounts[member.friendId] = amount / (Double(group.groupMember.count))
                percentages[member.friendId] = 0
            }
        case .percent:
            for member in group.groupMember {
                amounts[member.friendId] = 0
            }
        case .custom:
            for member in group.groupMember {
                amounts[member.friendId] = 0
                percentages[member.friendId] = 0
            }
        }
    }

    private func calculateTotals() {
        // Check if the amounts and percentages match the totals
        for member in group.groupMember {
            if paymentSplit == .equally || paymentSplit == .custom {
                if let amount = amounts[member.friendId] {
                    totalMemberAmounts += amount
                    print("MemberAmounts: \(totalMemberAmounts)")
                }
            } else if paymentSplit == .percent {
                if let percent = percentages[member.friendId] {
                    totalMemberPercentages += percent
                    print("MemberPercentages: \(totalMemberPercentages)")
                }
            }
        }

        if paymentSplit == .equally || paymentSplit == .custom {
            if totalMemberAmounts != amount {
                showSumAlert.toggle()
            }
        } else if paymentSplit == .percent {
            if totalMemberPercentages != 100 {
                showPercentAlert.toggle()
            }
        }
    }
}

#Preview {
    AddGroupPaymentView(group: .exampleGroup)
}
