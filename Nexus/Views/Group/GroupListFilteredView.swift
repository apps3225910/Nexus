//
//  GroupListFilteredView.swift
//  Nexus
//
//  Created by Pascal Hintze on 27.08.2024.
//

import SwiftUI
import Model

struct GroupListFilteredView: View {
    @Environment(\.managedObjectContext) private var viewContext

//    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(format: "isFriendship = %@", NSNumber(value: false))
//    ) var groups: FetchedResults<Model.Group>

    @FetchRequest(sortDescriptors: []) var groups: FetchedResults<Model.Group>

    init(searchText: String, sortAscending: Bool, selectedFilter: FilterOptions) {
//        let friendPredicate = NSPredicate(format: "isFriendship = %@", NSNumber(value: false))
        let namePredicate = NSPredicate(format: "name CONTAINS[c] %@", searchText)
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: sortAscending)
        var filterPredicate = NSPredicate(value: true)
        let isDeletedPredicate = NSPredicate(format: "isGroupDeleted == false")

        if selectedFilter == .open {
            filterPredicate = NSPredicate(format: "openAmount > 0")
        } else if selectedFilter == .closed {
            filterPredicate = NSPredicate(format: "openAmount == 0")
        }

        if searchText.isEmpty {
            _groups = FetchRequest<Model.Group>(sortDescriptors: [sortDescriptor],
                                                predicate: NSCompoundPredicate(andPredicateWithSubpredicates:
                                                                                [
                                                                                 filterPredicate,
                                                                                 isDeletedPredicate]))
        } else {
            _groups = FetchRequest<Model.Group>(sortDescriptors: [sortDescriptor],
                                                predicate: NSCompoundPredicate(andPredicateWithSubpredicates:
                                                                                [
                                                                                 namePredicate,
                                                                                 filterPredicate,
                                                                                 isDeletedPredicate]))
        }
    }

    var body: some View {
        List {
            ForEach(groups) { group in
                NavigationLink {
                    GroupDetailView(group: group)
                } label: {
                    GroupListRowView(group: group)
                }
            }
            .onDelete(perform: removeGroup)
        }
        .accessibilityIdentifier("FilteredGroupList")
    }

//    func delete(at offsets: IndexSet) {
//        for index in offsets {
//            let group = groups[index]
//            viewContext.delete(group)
//        }
//        PersistenceController.shared.save()
//    }

    private func removeGroup(at offsets: IndexSet) {
        for index in offsets {
            do {
                let group = groups[index]
                group.isGroupDeleted = true
                try viewContext.save()
            } catch {
                print("Failed to mark group as deleted")
            }
        }
    }
}

#Preview {
    GroupListFilteredView(searchText: "", sortAscending: false, selectedFilter: .all)
}
