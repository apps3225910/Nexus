//
//  AddGroupView.swift
//  Nexus
//
//  Created by Pascal Hintze on 19.07.2024.
//

import SwiftUI
import Model
import CoreData
import CloudKit

struct AddGroupView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss

    @State private var name = ""
    @State private var type: GroupTypes = .home
    @State private var selectedFriends = Set<GroupMember>()

    @State private var groupColor: GroupColors = .blue

    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(value: true)
    ) private var users: FetchedResults<Model.User>

//    @FetchRequest(sortDescriptors: []) var friends: FetchedResults<Friend>

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section("Name") {
                        TextField("Group Name", text: $name)
                    }

                    Section("Select Type") {
                        Picker("Type", selection: $type) {
                            ForEach(GroupTypes.allCases) { type in
                                Label(type.name, systemImage: type.icon).tag(type)
                            }
                        }
                        .pickerStyle(.automatic)
                    }

                    Section("Select Color") {
                        Picker(selection: $groupColor) {
                            ForEach(GroupColors.allCases) { color in
                                HStack {
                                    Text(color.name)
                                    Image(systemName: "circle.fill")
                                        .foregroundStyle(color.colorValue.opacity(0.5), .clear)
                                }
                                .tag(color)
                            }
                        } label: {
                            Label("Color", systemImage: "paintpalette")
                                .labelStyle(.iconOnly)
                        }
                        .pickerStyle(.automatic)
                    }
                }
            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel") {
                        dismiss()
                    }
                }

                ToolbarItem(placement: .confirmationAction) {
                    Button("Save") {
                        Task {

                            if let user = users.first {
                                let userAsGroupMember = GroupMember(context: viewContext)
                                userAsGroupMember.friendName = user.username
                                userAsGroupMember.amount = 0
                                userAsGroupMember.friendId = user.ckID
                                userAsGroupMember.picture = user.profilepicture

                            await PersistenceController.shared.addGroup(name: name,
                                                                        type: type,
                                                                        owner: userAsGroupMember,
                                                                        groupColor: groupColor,
                                                                        context: viewContext)
                        }
                        }
                        dismiss()
                    }
                }
            }
            .navigationTitle("New Group")
            .onChange(of: groupColor) {
                print(groupColor.colorValue)
            }
        }
    }

//    func delete(at offsets: IndexSet) {
//        for index in offsets {
//            let friend = friends[index]
//            viewContext.delete(friend)
//        }
//        PersistenceController.shared.save()
//    }
}

 #Preview {
    AddGroupView()
 }
