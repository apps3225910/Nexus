//
//  GroupDetailView.swift
//  Nexus
//
//  Created by Pascal Hintze on 19.07.2024.
//

import SwiftUI
import Model
import CoreData
import CloudKit

struct GroupDetailView: View {
    @Environment(\.dismiss) var dismiss
    @Environment(\.managedObjectContext) var viewContext

    @ObservedObject var group: Model.Group

    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(value: true)
    ) private var users: FetchedResults<Model.User>

    @State private var showAddGroupPaymentView = false

    @State private var selectedView: ViewPicker = .expenses

    @State private var showEditGroupView = false

    @State var attempToDismiss = UUID()
    @State var disable = true
    @State private var showCancelDialog = false

    enum ViewPicker: CaseIterable {
        case expenses
        case balances

        var name: String {
            switch self {
            case .expenses:
                return "Expenses"
            case .balances:
                return "Balances"
            }
        }
    }

    var body: some View {
        NavigationStack {
            VStack {
                HStack {
                    Image(systemName: group.groupType.icon)
                    Text(group.name)
                }
                .font(.title)
                .bold()

                Spacer()

                if group.groupMember.count == 1 {
                    ShareLink(item: group, preview: SharePreview(group.name)) {
                        Label("Add Group Members", systemImage: "person.badge.plus")
                    }
                } else {
                    Picker("Select View", selection: $selectedView) {
                        ForEach(ViewPicker.allCases, id: \.self) { item in
                            Text(item.name)
                        }
                    }
                    .pickerStyle(.segmented)
                    .padding()

                    if selectedView == .expenses {
                        ZStack(alignment: .bottom) {
                                GroupDetailExpensesView(group: group)
                            addPaymentButton()
                            }

                    } else {
                        ZStack(alignment: .bottom) {
                            GroupDetailBalancesView(group: group)
                            addPaymentButton()
                        }
                    }
                }

                Spacer()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            if let user = users.first {
                if user.ckID == group.owner {
                    ownerToolbar()
                } else {
                    memberToolbar()
                }
            }
        }
                .sheet(isPresented: $showAddGroupPaymentView) {
                    AddGroupPaymentView(group: group)
                }
    }

    @ViewBuilder
    private func addPaymentButton() -> some View {
        Button {
            showAddGroupPaymentView = true
        } label: {
            Image(systemName: "plus")
                .bold()
                .foregroundStyle(.white)
                .accessibilityIdentifier("AddPayment")
                .accessibilityLabel("Add Payment")
                .frame(width: 50, height: 50)
                .background(.blue)
                .clipShape(Circle())
        }
        .padding(.bottom, 20)
    }

    @ViewBuilder
    private func memberToolbar() -> some View {
        Menu {
            Section {
                NavigationLink("Settled Payments", destination: GroupSettledPaymentsView(group: group))
            }

            Button("Show activities") {

            }

            Button("Charts") { }

            Button(role: .destructive) {

            } label: {
                Label("Leave Group", systemImage: "person.crop.circle.badge.minus")
            }
        } label: {
            Label("Edit", systemImage: "ellipsis.circle")
        }

//        Button {
//            showAddGroupPaymentView = true
//        } label: {
//            Label("Add Payment", systemImage: "plus")
//        }
//        .sheet(isPresented: $showAddGroupPaymentView) {
//            AddGroupPaymentView(group: group)
//        }
    }

    @ViewBuilder
    private func ownerToolbar() -> some View {
        Menu {
            Section {
                Button {
                    showEditGroupView = true
                } label: {
                    Label("Edit Group", systemImage: "pencil")
                }

                NavigationLink("Settled Payments", destination: GroupSettledPaymentsView(group: group))

                Button("Show activities") {

                }

                ShareLink(item: group, message: Text("Peter would like to invite you to \(group.name)"),
                          preview: SharePreview(
                            group.name,
                            image: Image(uiImage: UIImage(systemName: group.groupType.icon) ?? UIImage())
                          )
                ) {
                    Label("Add Group Member", systemImage: "person.badge.plus")
                }
            }

            Button(role: .destructive) {
                Task {
                    group.isGroupDeleted = true
                    try viewContext.save()
                    dismiss()
                }
            } label: {
                Label("Delete Group", systemImage: "trash")
            }
        } label: {
            Label("Edit", systemImage: "ellipsis.circle")
        }
        .sheet(isPresented: $showEditGroupView) {
            EditGroupView(showCancelDialog: $showCancelDialog, group: group)
                .interactiveDismissDisabled(disable, attempToDismiss: $attempToDismiss)
                .onChange(of: attempToDismiss) {
                    showCancelDialog = true
                }
        }

//        Button {
//            showAddGroupPaymentView = true
//        } label: {
//            Label("Add Payment", systemImage: "plus")
//        }
//        .sheet(isPresented: $showAddGroupPaymentView) {
//            AddGroupPaymentView(group: group)
//        }
    }
}

#Preview {
    GroupDetailView(group: .exampleGroup)
}
