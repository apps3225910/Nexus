//
//  EditGroupView.swift
//  Nexus
//
//  Created by Pascal Hintze on 26.10.2024.
//

import SwiftUI
import Model
import CoreData
import CloudKit

struct EditGroupView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss

    @Binding var showCancelDialog: Bool

    @ObservedObject var group: Model.Group

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section("Name") {
                        TextField(group.name, text: $group.name)
                    }

                    Section("Type") {
                        Picker("Type", selection: $group.type) {
                            ForEach(GroupTypes.allCases) { type in
                                Label(type.name, systemImage: type.icon).tag(type)
                            }
                        }
                        .pickerStyle(.automatic)
                    }

                    Section("Color") {
                        Picker(selection: $group.color) {
                            ForEach(GroupColors.allCases) { color in
                                HStack {
                                    Text(color.name)
                                    Image(systemName: "circle.fill")
                                        .foregroundStyle(color.colorValue.opacity(0.5), .clear)
                                }
                                .tag(color)
                            }
                        } label: {
                            Label("Color", systemImage: "paintpalette")
                                .labelStyle(.iconOnly)
                        }
                        .pickerStyle(.automatic)
                    }

                    Section("Members") {
                        List {
                            ForEach(Array(group.groupMember.sorted(by: {$0.friendName < $1.friendName})),
                                    id: \.self) { member in
                                groupMemberRows(member: member)
                                    .alignmentGuide(.listRowSeparatorLeading) { $0[.leading] }
                            }
                                    .onDelete(perform: delete)
                        }
                    }
                }
            }
            .onAppear {
                viewContext.undoManager?.beginUndoGrouping() // Start an undo group
            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel") {
                        hideKeyboard()
                        showCancelDialog = true

                    }
                    .confirmationDialog("Are you sure you want to discard your changes?",
                                        isPresented: $showCancelDialog, titleVisibility: .visible) {

                        Button("Discard Changes", role: .destructive) {

                            viewContext.undoManager?.endUndoGrouping() // End the undo group
                            viewContext.undoManager?.undo() // Undo all changes in this group
                            viewContext.refresh(group, mergeChanges: false)

                            dismiss()
                        }

                        Button("Keep editing", role: .cancel) { }
                    }
                }

                ToolbarItem(placement: .confirmationAction) {
                    Button("Save") {
                        viewContext.undoManager?.endUndoGrouping() // End the undo group
                        Task {
                            try viewContext.save()
                        }
                        dismiss()
                    }
                }
            }
            .navigationTitle("Edit Group")
        }
    }

    func delete(at offsets: IndexSet) {
        for index in offsets {
            let member = group.groupMember.sorted(by: {$0.friendName < $1.friendName})[index]
            viewContext.delete(member)
        }
        PersistenceController.shared.save()
    }

    @ViewBuilder
    private func groupMemberRows(member: GroupMember) -> some View {
        HStack {
            Image(uiImage: UIImage(data: member.picture) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 40, height: 40)
                .clipShape(Circle())
                .padding(.trailing, 10)

            Text(member.friendName)
                .font(.headline)

            Text("\(member.amount.formatted(.currency(code: "USD")))")
                .font(.headline)
        }
    }
}

#Preview {
    EditGroupView(showCancelDialog: .constant(false), group: .exampleGroup)
}
