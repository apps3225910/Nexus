//
//  GroupListView.swift
//  Nexus
//
//  Created by Pascal Hintze on 19.07.2024.
//

import SwiftUI
import Model
import CloudKit
import CoreData

struct GroupListView: View {
    @Environment(\.managedObjectContext) private var viewContext

//    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(format: "isFriendship = %@", NSNumber(value: false))
//    ) var groups: FetchedResults<Model.Group>

    @FetchRequest(sortDescriptors: []) var groups: FetchedResults<Model.Group>

    @State private var showGroupAddView = false
    @State private var searchText = ""
    @State private var sortAscending = true
    @State private var selectedFilter: FilterOptions = .all

    var body: some View {
        NavigationStack {
            VStack {
                if groups.isEmpty {
                    ContentUnavailableView {
                        Label("No Groups", systemImage: "doc.text")
                    } description: {
                        Text("Your groups will appear here.")
                    }
                } else {
                    GroupListFilteredView(searchText: searchText, sortAscending: sortAscending,
                                          selectedFilter: selectedFilter)
                        .searchable(text: $searchText, prompt: Text("Groups"))
                }
            }
            .toolbar {
                GroupListToolbarView(showGroupAddView: $showGroupAddView, sortAscending: $sortAscending,
                                     selectedFilter: $selectedFilter)
            }
            .navigationTitle("Groups")
        }
        .onReceive(NotificationCenter.default.storeDidChangePublisher) { notification in
            processStoreChangeNotification(notification)
        }
    }

    /**
     Merge the transactions, if any.
     */
    private func processStoreChangeNotification(_ notification: Notification) {
        let transactions = PersistenceController.shared.groupTransactions(from: notification)
        if !transactions.isEmpty {
            PersistenceController.shared.mergeTransactions(transactions, to: viewContext)
        }
    }
}

#Preview {
    GroupListView()
}
