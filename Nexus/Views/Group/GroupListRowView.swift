//
//  GroupListRowView.swift
//  Nexus
//
//  Created by Pascal Hintze on 22.08.2024.
//

import SwiftUI
import Model

struct GroupListRowView: View {
    @ObservedObject var group: Model.Group
    let lastPayment: Payment?
    let groupMember: GroupMember?

    init(group: Model.Group) {
        self.group = group

        let filteredPayments = group.payment.filter { !$0.isPaymentDeleted }
        let sortedPayments = filteredPayments.sorted(by: { $0.entryDate > $1.entryDate })
        lastPayment = sortedPayments.first

        if let owner = lastPayment?.paymentOwner {
            groupMember = (group.groupMember.first(where: {$0.friendId == owner}))
        } else {
            groupMember = nil
        }
    }

    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 12.0)
                    .frame(width: 60, height: 60)
                    .foregroundStyle(group.groupColor.colorValue.gradient.opacity(0.5))

                Image(systemName: group.groupType.icon)
            }

            VStack(alignment: .leading) {

                Text(group.name)
                        .font(.headline)

                if let payment = lastPayment {
                    if let paymentOwner = groupMember {
                        Group {
                            Text("\(paymentOwner.friendName) added: \(payment.paymentDescription)")
                            + Text(" \(payment.amount.formatted(.currency(code: "USD")))")
                        }
                        .font(.footnote)
                    }
                }
            }

            Text(group.id.uuidString)
        }
    }
}

 #Preview {
     GroupListRowView(group: .exampleGroup)
         .environment(\.managedObjectContext, PersistenceController(inMemory: true).persistentContainer.viewContext)

 }
