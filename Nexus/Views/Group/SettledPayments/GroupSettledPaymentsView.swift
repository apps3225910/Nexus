//
//  GroupSettledPaymentsView.swift
//  Nexus
//
//  Created by Pascal Hintze on 12.11.2024.
//

import SwiftUI
import Model

struct GroupSettledPaymentsView: View {
    @Environment(\.managedObjectContext) private var viewContext
    let group: Model.Group

    @State private var groupedPayments: [String: [SettledPayment]] = [:]

    var body: some View {
        NavigationStack {
            VStack {
                Spacer()

                if groupedPayments.isEmpty {
                    ContentUnavailableView {
                        Label("No settled payments", systemImage: "doc.text")
                    } description: {
                        Text("The groups settled payments will appear here.")
                    }
                    .frame(height: 200)

                    Spacer()

                } else {
                    List {
                        ForEach(groupedPayments.keys.sorted(by: >), id: \.self) { monthYear in
                            Section(header: Text(monthYear)) {
                                ForEach(groupedPayments[monthYear]!) { payment in
                                    NavigationLink {
                                        GroupSettledPaymentDetailView(settledPayment: payment, group: group)
                                    } label: {
                                        listRow(settledPayment: payment)
                                    }
                                }
                                .onDelete { indexSet in
                                    delete(at: indexSet, for: monthYear)
                                }
                            }
                            .listRowSeparator(.hidden)
                        }
                        .listRowBackground(Color(.lightGray).opacity(0.1))
                    }
                    .scrollContentBackground(.hidden)
                    .listRowSpacing(10)
                }
            }
            .onAppear {
                groupedPayments = Dictionary(
                    grouping: group.settledPayment
                        .filter { !$0.isSettledPaymentDeleted } // Filter out deleted payments
                        .sorted { $0.date > $1.date }           // Sort by date
                ) { payment in
                    payment.date.formattedMonthAndYear()       // Group by formatted date
                }
            }
            .navigationTitle("Settled Payments")
        }
    }

    private func delete(at offsets: IndexSet, for monthYear: String) {
        guard let paymentsForMonth = groupedPayments[monthYear] else { return }

        // Find the payment to delete using the offsets
        for index in offsets {
            let paymentToDelete = paymentsForMonth[index]

            // Remove the payment from the original array
            if let paymentIndex = group.settledPayment.firstIndex(where: { $0.id == paymentToDelete.id }) {
                let settledPayment = group.settledPayment[paymentIndex]
                settledPayment.isSettledPaymentDeleted = true
                PersistenceController.shared.save()
            }
        }
    }

    @ViewBuilder
    private func listRow(settledPayment: SettledPayment) -> some View {
        HStack {
            VStack {
                Text("\(settledPayment.date, format: .dateTime.day(.twoDigits))")
                Text("\(settledPayment.date, format: .dateTime.month())")
            }

            Text("\(settledPayment.payerName) paid \(settledPayment.payeeName)")
                .padding(.leading)

            Spacer()

            Text("\(abs(settledPayment.amount).formatted(.currency(code: "USD")))")

            Image(systemName: "paperclip")
                .foregroundStyle(settledPayment.attachment != Data() ? .blue : .gray )

            Image(systemName: "text.bubble")
                .foregroundStyle(settledPayment.comment != "" ? .blue : .gray )
        }
    }
}

#Preview {
    GroupSettledPaymentsView(group: .exampleGroup)
}
