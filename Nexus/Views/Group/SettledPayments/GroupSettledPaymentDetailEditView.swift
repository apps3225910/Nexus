//
//  GroupSettledPaymentDetailEditView.swift
//  Nexus
//
//  Created by Pascal Hintze on 21.01.2025.
//

import SwiftUI
import Model
import CoreData

struct GroupSettledPaymentDetailEditView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.presentationMode) private var presentationMode // Add this

    @ObservedObject var settledPayment: SettledPayment

    let currencies = Locale.Currency.isoCurrencies

    let group: Model.Group

    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(value: true)
    ) private var users: FetchedResults<Model.User>

    @State private var payerPicture = Data()
    @State private var payeePicture = Data()

    @Binding var showCancelDialog: Bool

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    VStack(alignment: .center) {
                        pictures(payerName: settledPayment.payerName, payerPicture: payerPicture,
                                 payeeName: settledPayment.payeeName, payeePicture: payeePicture)
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                }
                .alignmentGuide(.listRowSeparatorLeading) { $0[.leading] }

                LabeledContent {
                    TextField("Amount payed", value: $settledPayment.amount,
                              format: .number.precision(.fractionLength(2)),
                              prompt: Text("Select amount"))
                    .keyboardType(.decimalPad)
                    .multilineTextAlignment(.trailing)
                } label: {
                    Text("Amount")
                }

                Picker("Currency", selection: $settledPayment.currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                DatePicker("Date", selection: $settledPayment.date, displayedComponents: .date)

                Section {
                    TextField("Comment", text: $settledPayment.comment, axis: .vertical)
                        .lineLimit(5, reservesSpace: true)
                }
            }

            .onAppear {
                payerPicture = getPicture(for: settledPayment.payer)
                payeePicture = getPicture(for: settledPayment.payee)
                viewContext.undoManager?.beginUndoGrouping() // Start an undo group
            }

            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button("Done") {
                        viewContext.undoManager?.endUndoGrouping() // End the undo group
                        Task {
                            try viewContext.save()

                            dismiss()
                        }
                    }
                }

                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel", role: .cancel) {
                        hideKeyboard()
                        showCancelDialog = true
                    }
                }
            }
            .confirmationDialog("Are you sure you want to discard your changes?",
                                isPresented: $showCancelDialog, titleVisibility: .visible) {

                Button("Discard Changes", role: .destructive) {
                    viewContext.undoManager?.endUndoGrouping() // End the undo group
                    viewContext.undoManager?.undo() // Undo all changes in this group
                    viewContext.refresh(settledPayment, mergeChanges: false)
                    dismiss()
                }

                Button("Keep editing", role: .cancel) { }
            }
        }
    }

    private func getPicture(for memberId: String) -> Data {
        guard let fetchedMember = group.groupMember.first(where: { $0.friendId == memberId }) else {
            return Data()
        }

        let memberPicture = fetchedMember.picture

        return memberPicture
    }

    @ViewBuilder
    private func pictures(payerName: String, payerPicture: Data?, payeeName: String, payeePicture: Data?) -> some View {
        HStack {
            Image(uiImage: UIImage(data: payerPicture ?? Data()) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .padding(.trailing, 10)

            Image(systemName: "arrow.right")
                .resizable()
                .scaledToFill()
                .frame(width: 30, height: 30)
                .padding(.trailing, 10)

            Image(uiImage: UIImage(data: payeePicture ?? Data()) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .padding(.trailing, 10)
        }
        Text("\(payerName) payed \(payeeName)")
    }
}

#Preview {
    GroupSettledPaymentDetailEditView(settledPayment: .example,
                                      group: .exampleGroup,
                                      showCancelDialog: .constant(false))
}
