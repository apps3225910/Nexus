//
//  AddSettlePaymentView.swift
//  Nexus
//
//  Created by Pascal Hintze on 29.10.2024.
//

import SwiftUI
import Model

struct AddSettlePaymentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss

    @Binding var selectedMember: MemberBalance

    @State private var currency: String = "USD"
    @State private var payedAmount: Double = 0
    @State private var comment: String = ""
    @State private var selectedDate: Date = Date()
    @State private var attachment: Data = Data()

    @State private var payer: String = ""
    @State private var payerName: String = ""
    @State private var payerPicture: Data = Data()

    @State private var payee: String = ""
    @State private var payeePicture: Data = Data()
    @State private var payeeName: String = ""

    let currencies = Locale.Currency.isoCurrencies
    let group: Model.Group
    let member: GroupMember

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    VStack(alignment: .center) {
                        pictures(payerName: payerName, payerPicture: payerPicture,
                                 payeeName: payeeName, payeePicture: payeePicture)
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                }
                .alignmentGuide(.listRowSeparatorLeading) { $0[.leading] }

                LabeledContent {
                    TextField("Amount payed", value: $payedAmount, format: .currency(code: currency),
                              prompt: Text("Select amount"))
                        .keyboardType(.decimalPad)
                        .multilineTextAlignment(.trailing)
                } label: {
                    Text("Amount")
                }

                Picker("Currency", selection: $currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                DatePicker("Date", selection: $selectedDate, displayedComponents: .date)

                Section {
                    TextField("Comment", text: $comment, axis: .vertical)
                        .lineLimit(5, reservesSpace: true)
                }
                .padding()
                .onAppear {
                    payedAmount = selectedMember.amountOwedOrDue

                    if selectedMember.amountOwedOrDue >= 0 {
                        payer = member.friendId
                        payerName = member.friendName
                        payerPicture = member.picture

                        payee = selectedMember.otherMember
                        payeeName = selectedMember.otherMemberName
                        payeePicture = selectedMember.otherMemberPicture
                    } else {
                        payer = selectedMember.otherMember
                        payerName = selectedMember.otherMemberName
                        payerPicture = selectedMember.otherMemberPicture

                        payee = member.friendId
                        payeeName = member.friendName
                        payeePicture = member.picture
                    }
                }
            }

            .navigationTitle("Record a payment")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {
                    Button("Save") {
                        Task {
                            let paymentInfo = SettledPaymentInfo(amount: payedAmount, paymentDate: selectedDate,
                                                          payer: payer, payerName: payerName, payee: payee,
                                                          payeeName: payeeName, comment: comment,
                                                                 attachment: attachment, currency: currency)

                            await PersistenceController.shared.createSettledPayment(
                                paymentInfo: paymentInfo,
                                group: group,
                                context: viewContext
                            )
                        }
                        dismiss()
                        selectedMember = MemberBalance(otherMember: "",
                                                       otherMemberPicture: Data(),
                                                       otherMemberName: "",
                                                       amountOwedOrDue: 0.0
                        )
                    }
                }
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel") {
                        dismiss()
                        selectedMember = MemberBalance(otherMember: "",
                                                       otherMemberPicture: Data(),
                                                       otherMemberName: "",
                                                       amountOwedOrDue: 0.0
                        )
                    }
                }
            }
        }
    }

    @ViewBuilder
    private func pictures(payerName: String, payerPicture: Data?, payeeName: String, payeePicture: Data?) -> some View {
            HStack(spacing: 50) {
                VStack {
                    Image(uiImage: UIImage(data: payerPicture ?? Data()) ?? UIImage())
                        .resizable()
                        .scaledToFill()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())

                    Text("\(payerName)")
                        .font(.caption)
                        .foregroundColor(.gray)
                }

                VStack {
                    Image(systemName: "arrow.right")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30)
                        .foregroundColor(.blue)

                    Text("payed")
                        .font(.caption)
                        .foregroundColor(.gray)
                }

                VStack {
                    Image(uiImage: UIImage(data: payeePicture ?? Data()) ?? UIImage())
                        .resizable()
                        .scaledToFill()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())

                    Text("\(payeeName)")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
    }
}

#Preview {
    AddSettlePaymentView(selectedMember: .constant(MemberBalance(otherMember: "",
                                                              otherMemberPicture: Data(),
                                                              otherMemberName: "Sara",
                                                              amountOwedOrDue: 5.0)),
                      group: .exampleGroup,
                      member: .example)
}
