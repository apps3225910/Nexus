//
//  GroupSettledPaymentDetailView.swift
//  Nexus
//
//  Created by Pascal Hintze on 15.11.2024.
//

import SwiftUI
import Model

struct GroupSettledPaymentDetailView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(\.managedObjectContext) private var viewContext

    @ObservedObject var settledPayment: SettledPayment

    @State private var showEditSettlementView = false
    @State private var showDeletionDialog = false

    let currencies = Locale.Currency.isoCurrencies

    let group: Model.Group

    @FetchRequest(sortDescriptors: [], predicate: NSPredicate(value: true)
    ) private var users: FetchedResults<Model.User>

    @State private var payerPicture = Data()
    @State private var payeePicture = Data()

    @State var attempToDismiss = UUID()
    @State var disable = true
    @State private var showCancelDialog = false

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    VStack(alignment: .center) {
                        pictures(payerName: settledPayment.payerName, payerPicture: payerPicture,
                                 payeeName: settledPayment.payeeName, payeePicture: payeePicture)
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                }
                .alignmentGuide(.listRowSeparatorLeading) { $0[.leading] }

                LabeledContent {
                    TextField("Amount payed", value: $settledPayment.amount,
                              format: .number.precision(.fractionLength(2)),
                              prompt: Text("Select amount"))
                    .keyboardType(.decimalPad)
                    .multilineTextAlignment(.trailing)
                } label: {
                    Text("Amount")
                }

                Picker("Currency", selection: $settledPayment.currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                DatePicker("Date", selection: $settledPayment.date, displayedComponents: .date)

                Section {
                    TextField("Comment", text: $settledPayment.comment, axis: .vertical)
                        .lineLimit(5, reservesSpace: true)
                }
            }
            .disabled(true)
            .onAppear {
                payerPicture = getPicture(for: settledPayment.payer)
                payeePicture = getPicture(for: settledPayment.payee)
            }
            .toolbar {
                // If the current user is part of the settled payment he is allowed to delete it
                if let user = users.first {
                    if user.ckID == settledPayment.payer || user.ckID == settledPayment.payee {
                        Menu {
                            Button(role: .destructive) {
                                showDeletionDialog = true
                            } label: {
                                Label("Delete", systemImage: "trash")
                            }
                        } label: {
                            Label("Options", systemImage: "ellipsis.circle")
                        }
                    }
                }

                Button("Edit") {
                    showEditSettlementView = true
                }
                .sheet(isPresented: $showEditSettlementView) {
                    GroupSettledPaymentDetailEditView(settledPayment: settledPayment,
                                                      group: group,
                                                      showCancelDialog: $showCancelDialog)
                        .interactiveDismissDisabled(disable, attempToDismiss: $attempToDismiss)
                        .onChange(of: attempToDismiss) {
                            showCancelDialog = true
                        }
                }
            }
            .confirmationDialog("Are you sure you want to delete this entry? This action can´t be undone.",
                                isPresented: $showDeletionDialog, titleVisibility: .visible) {

                Button("Delete Entry", role: .destructive) {
                    settledPayment.isSettledPaymentDeleted = true
                    dismiss()
                }
            }
        }
    }

    private func getPicture(for memberId: String) -> Data {
        guard let fetchedMember = group.groupMember.first(where: { $0.friendId == memberId }) else {
            return Data()
        }

        let memberPicture = fetchedMember.picture

        return memberPicture
    }

    @ViewBuilder
    private func pictures(payerName: String, payerPicture: Data?, payeeName: String, payeePicture: Data?) -> some View {
        HStack {
            Image(uiImage: UIImage(data: payerPicture ?? Data()) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .padding(.trailing, 10)

            Image(systemName: "arrow.right")
                .resizable()
                .scaledToFill()
                .frame(width: 30, height: 30)
                .padding(.trailing, 10)

            Image(uiImage: UIImage(data: payeePicture ?? Data()) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .padding(.trailing, 10)
        }
        Text("\(payerName) payed \(payeeName)")
    }
}

#Preview {
    GroupSettledPaymentDetailView(settledPayment: .example, group: .exampleGroup)
}
