//
//  GroupMemberBalancesView.swift
//  Nexus
//
//  Created by Pascal Hintze on 28.10.2024.
//

import SwiftUI
import Model
import CoreData

struct GroupMemberBalancesView: View {
    let group: Model.Group
    let member: GroupMember?

    @State private var showSettleSheet = false

    @State private var selectedMember: MemberBalance = MemberBalance(
        otherMember: "",
        otherMemberPicture: Data(),
        otherMemberName: "",
        amountOwedOrDue: 0.0)

    @State private var details: [(String, Double)] = []

    var body: some View {
        VStack {
            if details.isEmpty {
                    ContentUnavailableView {
                        Label("No balances for \(member?.friendName ?? "")", systemImage: "doc.text")
                    } description: {
                        Text("The balances for \(member?.friendName ?? "") will appear here.")
                    }
                    .frame(height: 200)
            } else {
                List(details, id: \.0) { (otherMemberId, amount) in
                    HStack {
                        if let otherMemberName = group.groupMember.first(where: { $0.friendId == otherMemberId }) {
                            if let member {
                                if amount > 0 {
                                    Text("\(member.friendName) needs to pay \(otherMemberName.friendName)")

                                    Spacer()

                                    Text("\(abs(amount).formatted(.currency(code: "USD")))")
                                        .foregroundStyle(.red)
                                } else {
                                    Text("\(otherMemberName.friendName) owes \(member.friendName)")

                                    Spacer()

                                    Text("\(abs(amount).formatted(.currency(code: "USD")))")
                                        .foregroundStyle(.green)
                                }
                            }
                        }

                        Spacer()

                        Button("Settle") {
                            if let otherMember = group.groupMember.first(where: { $0.friendId == otherMemberId }) {

                                selectedMember = MemberBalance(otherMember: otherMember.friendId,
                                                               otherMemberPicture: otherMember.picture,
                                                               otherMemberName: otherMember.friendName,
                                                               amountOwedOrDue: amount
                                )
                                showSettleSheet.toggle()
                            }
                        }
                        .buttonStyle(.borderedProminent)
                    }
                }
                .sheet(isPresented: $showSettleSheet) {
                    if let member {
                        AddSettlePaymentView(selectedMember: $selectedMember, group: group, member: member)
                    }
                }
            }
        }
        .onAppear {
            if let member {
                details = PersistenceController.shared.fetchMemberDetails(for: member, in: group)
            }
        }
        .navigationTitle("\(member?.friendName ?? "") Balances")
    }
}

#Preview {
    GroupMemberBalancesView(group: .exampleGroup, member: .example)
}
