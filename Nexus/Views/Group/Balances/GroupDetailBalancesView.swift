//
//  GroupDetailBalancesView.swift
//  Nexus
//
//  Created by Pascal Hintze on 13.08.2024.
//

import CloudKit
import SwiftUI
import Model
import CoreData

struct GroupDetailBalancesView: View {
    let group: Model.Group

    @State private var showSettlePayment = false
    @State private var memberBalances: [String: Double] = [:]

    var body: some View {
        List {
            ForEach(Array(group.groupMember.sorted(by: {$0.friendName < $1.friendName})), id: \.self) { member in
                NavigationLink {
                    GroupMemberBalancesView(group: group, member: member)
                } label: {
                    GroupDetailBalancesListRowView(member: member, balance: memberBalances[member.friendId] ?? 0.0)
                }
                .listRowSeparator(.hidden)
            }
            .listRowBackground(Color(.lightGray).opacity(0.1))
        }
        //            .scrollDisabled(true)
        .scrollContentBackground(.hidden)
        .listRowSpacing(10)
        .onAppear {
            self.memberBalances = PersistenceController.shared.fetchGroupMemberBalances(in: group)
        }
    }
}

#Preview {
    GroupDetailBalancesView(group: .exampleGroup)
}
