//
//  GroupDetailBalancesListRowView.swift
//  Nexus
//
//  Created by Pascal Hintze on 13.08.2024.
//

import CloudKit
import SwiftUI
import Model

struct GroupDetailBalancesListRowView: View {
    let member: GroupMember
    let balance: Double

    var body: some View {
        HStack {
            Image(uiImage: UIImage(data: member.picture) ?? UIImage())
                .resizable()
                .scaledToFill()
                .frame(width: 50, height: 50)
                .clipShape(Circle())

            Text(member.friendName)
                .font(.headline)

            Spacer()

            Text("\(balance.formatted(.currency(code: "USD")))")
                .foregroundColor(balance >= 0 ? .green : .red)
        }
    }
}

#Preview {
    GroupDetailBalancesListRowView(member: .example, balance: 10.50)
}
