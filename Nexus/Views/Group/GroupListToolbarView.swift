//
//  GroupListToolbarView.swift
//  Nexus
//
//  Created by Pascal Hintze on 27.08.2024.
//

import SwiftUI
import Model

struct GroupListToolbarView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Binding var showGroupAddView: Bool
    @Binding var sortAscending: Bool
    @Binding var selectedFilter: FilterOptions

    var body: some View {
        HStack {
            Menu {
                Picker("Sort by", selection: $sortAscending) {
                    Text("A to Z").tag(true)
                    Text("Z to A").tag(false)
                }
            }  label: {
                Label("Sort", systemImage: "ellipsis.circle.fill")
                    .foregroundStyle((sortAscending ? .blue : .white ), (sortAscending ? .gray.opacity(0.2) : .blue))
                    .labelStyle(.iconOnly)
            }

            Menu {
                Picker("Sort by", selection: $selectedFilter) {
                    ForEach(FilterOptions.allCases) { option in
                        if let icon = option.icon {
                            Label(option.rawValue, systemImage: icon).tag(option)
                        } else {
                            Text(option.rawValue).tag(option)
                        }
                    }
                }
            } label: {
                Label("Filter", systemImage: "line.3.horizontal.decrease.circle.fill")
                    .foregroundStyle((selectedFilter == .all ? .blue : .white ),
                                     (selectedFilter == .all ? .gray.opacity(0.2) : .blue))
                    .labelStyle(.iconOnly)
            }

#if DEBUG
            Menu {
                Button("Generate Data") {
                    Task {
                        try await PersistenceController.shared.generateData(context: viewContext)
                    }
                }
                .accessibilityIdentifier("Generate Data")
                
                Button("Delete Data") {
                    Task {
                        PersistenceController.shared.deleteLargeData(context: viewContext)
                    }
                }
                .accessibilityIdentifier("Delete Data")
            } label: {
                Text("Data generator")
            }
#endif

            Button {
                showGroupAddView = true
            } label: {
                Label("Add Group", systemImage: "plus.circle.fill")
                    .foregroundStyle(.blue, .gray.opacity(0.2))
                    .labelStyle(.iconOnly)
            }
            .sheet(isPresented: $showGroupAddView) {
                AddGroupView()
            }
        }
    }
}

#Preview {
    GroupListToolbarView(showGroupAddView: .constant(false),
                         sortAscending: .constant(false),
                         selectedFilter: .constant(.all))
}
