//
//  GroupExpensesDetailView.swift
//  Nexus
//
//  Created by Pascal Hintze on 25.07.2024.
//

import SwiftUI
import CloudKit
import Model

struct GroupExpensesDetailView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss

    @State private var paidBy: GroupMember?

    @State private var isPresentingEditView = false

    @ObservedObject var group: Model.Group
    @ObservedObject var payment: Payment

    let currencies = Locale.Currency.isoCurrencies

    @State var attempToDismiss = UUID()
    @State var disable = true
    @State private var showCancelDialog = false

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    Text("\(payment.paymentDescription)")

                    Spacer()

                    Picker("Type", selection: $payment.category) {
                        ForEach(PaymentCategory.allCases, id: \.self) { type in
                            Label(type.name, systemImage: type.icon)
                                .labelStyle(.iconOnly)
                        }
                    }
                    .labelsHidden()
                }

                HStack {
                    Text("Amount")
                    Spacer()
                    Text("\(payment.amount.formatted(.number))")
                        .multilineTextAlignment(.trailing)
                }

                Picker("Currency", selection: $payment.currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                HStack {
                    Text("Date")
                    Spacer()
                    Text("\(payment.entryDate.formatted(date: .abbreviated, time: .omitted))")
                }

                Section("Paid by") {
                    Picker("Paid by", selection: $paidBy) {
                        ForEach(Array(group.groupMember), id: \.self) { member in
                            Text(member.friendName).tag(member as GroupMember?)
                        }
                    }
                }

                Section("Splitted") {
                    Picker("Splitted", selection: $payment.split) {
                        ForEach(PaymentSplit.allCases, id: \.self) { split in
                            Text(split.name).tag(split)
                        }
                    }
                    .pickerStyle(.segmented)
                }

                SplitView(payment: payment, group: group, isDisabled: true)

            }
            .onAppear {
                paidBy = group.groupMember.first { $0.friendId == payment.paymentOwner }
            }
            .disabled(true)
            .toolbar {
                Menu {
                    Button(role: .destructive) {
                        Task {
                            payment.isPaymentDeleted = true
                            try viewContext.save()
                            dismiss()
                        }
                    } label: {
                        Label("Delete", systemImage: "trash")
                    }
                } label: {
                    Label("Options", systemImage: "ellipsis.circle")
                }
                Button("Edit") {
                    isPresentingEditView = true
                }
            }
            .sheet(isPresented: $isPresentingEditView) {
                GroupExpensesDetailEditView(showCancelDialog: $showCancelDialog,
                                            group: group,
                                            payment: payment)
                .interactiveDismissDisabled(disable, attempToDismiss: $attempToDismiss)
                .onChange(of: attempToDismiss) {
                    showCancelDialog = true
                }
            }

            .navigationTitle(payment.paymentDescription)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#Preview {
    GroupExpensesDetailView(group: .exampleGroup, payment: .exampleGroup)
}
