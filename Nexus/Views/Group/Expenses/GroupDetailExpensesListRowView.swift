//
//  GroupDetailExpensesListRowView.swift
//  Nexus
//
//  Created by Pascal Hintze on 09.08.2024.
//

import SwiftUI
import CloudKit
import Model

struct GroupDetailExpensesListRowView: View {
    @ObservedObject var payment: Payment

    var body: some View {
        HStack {
            VStack {
                Text("\(payment.entryDate, format: .dateTime.day(.twoDigits))")
                Text("\(payment.entryDate, format: .dateTime.month())")
            }

            Image(systemName: payment.category.icon)
                .resizable()
                .scaledToFit()
                .foregroundStyle(payment.category.color.opacity(0.7))
                .frame(width: 35, height: 35)
                .padding(.leading, 10)

            Text("\(payment.paymentDescription)")

            Spacer()

            VStack {
                Text("\(payment.paymentOwnerName) paid")
                    .font(.footnote)

                Text("\(payment.amount.formatted(.currency(code: payment.currency)))")
            }
        }
    }
}

#Preview {
    GroupDetailExpensesListRowView(payment: .exampleGroup)
}
