//
//  GroupDetailExpensesView.swift
//  Nexus
//
//  Created by Pascal Hintze on 13.08.2024.
//

import CloudKit
import Model
import SwiftUI

struct GroupDetailExpensesView: View {
    @Environment(\.managedObjectContext) private var viewContext

    let group: Model.Group

    @FetchRequest var payments: FetchedResults<Payment>

//    @FetchRequest var allPayments: FetchedResults<Payment>

    init(group: Model.Group) {
        self.group = group

        let groupPredicate = NSPredicate(format: "group == %@", group)
        let isDeletedPredicate = NSPredicate(format: "isPaymentDeleted == false")

        _payments = FetchRequest(
            entity: Payment.entity(),
            sortDescriptors: [NSSortDescriptor(keyPath: \Payment.entryDate, ascending: false)],
            predicate: NSCompoundPredicate(type: .and, subpredicates: [groupPredicate, isDeletedPredicate])
        )

//        _allPayments = FetchRequest(
//            entity: Payment.entity(),
//            sortDescriptors: [NSSortDescriptor(keyPath: \Payment.entryDate, ascending: false)],
//            predicate: NSCompoundPredicate(type: .and, subpredicates: [groupPredicate])
//        )
    }

    // Grouping payments by month and year
    var groupedPayments: [String: [Payment]] {
        Dictionary(grouping: payments.sorted { $0.entryDate > $1.entryDate }) {
            $0.entryDate.formattedMonthAndYear()
        }
    }

    var body: some View {
        VStack {
            if groupedPayments.isEmpty {
                Spacer()
                ContentUnavailableView {
                    Label("No payments", systemImage: "doc.text")
                } description: {
                    Text("The group payments will appear here.")
                }
                .frame(height: 200)

                Spacer()
            } else {
                List {
                // TODO: 2024 is displayed before 2025 needs to be fixed
                    // Iterate over the grouped payments by month and year
                    ForEach(groupedPayments.keys.sorted(by: <), id: \.self) { yearMonth in
                        Section(header: Text(yearMonth)) {
                            if let paymentsForMonth = groupedPayments[yearMonth] {
                                ForEach(paymentsForMonth) { payment in
                                    NavigationLink {
                                        GroupExpensesDetailView(group: group, payment: payment)
                                    } label: {
                                        GroupDetailExpensesListRowView(payment: payment)
                                    }
                                }
                            }
                        }
                    }
                    .onDelete(perform: removePayment)
                    .listRowSeparator(.hidden)
                    .listRowBackground(Color(.lightGray).opacity(0.1))
                }
//                List {
//                    ForEach(payments) { payment in
//                        NavigationLink {
//                                GroupExpensesDetailView(group: group, payment: payment)
//                        } label: {
//                                GroupDetailExpensesListRowView(payment: payment)
//                        }
//                    }
//                    .onDelete(perform: removePayment)
//                    .listRowSeparator(.hidden)
//                    .listRowBackground(Color(.lightGray).opacity(0.1))
//                }
                .scrollContentBackground(.hidden)
                .listRowSpacing(10)
            }
        }
    }

    private func removePayment(at offsets: IndexSet) {
        for index in offsets {
            do {
                let payment = payments[index]
                payment.isPaymentDeleted = true
                try viewContext.save()
            } catch {
                print("Failed to mark payment as deleted")
            }
        }
    }
}

#Preview {
    GroupDetailExpensesView(group: Model.Group.exampleGroup)
}
