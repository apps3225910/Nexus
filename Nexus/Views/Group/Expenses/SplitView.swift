//
//  SplitView.swift
//  Nexus
//
//  Created by Pascal Hintze on 05.01.2025.
//

import SwiftUI
import Model

struct SplitView: View {
    @ObservedObject var payment: Payment
    @ObservedObject var group: Model.Group

    let isDisabled: Bool

    var body: some View {
        Group {
            switch PaymentSplit(rawValue: payment.paymentSplit) {
            case .equally:
                ForEach(Array(group.groupMember), id: \.self) { member in
                    HStack {
                        LabeledContent {
                            TextField("Amount", value: Binding(
                                get: {
                                    payment.amountFor(memberId: member.friendId) ?? 0.0
                                },
                                set: { newValue in
                                    payment.updateAmount(for: member.friendId, newAmount: newValue)
                                }
                            ), format: .number)
                            .multilineTextAlignment(.trailing)
                            Text(payment.currency)
                        } label: {
                            Text(member.friendName)
                        }
                    }
                }
            case .percent:
                ForEach(Array(group.groupMember), id: \.self) { member in
                    HStack {
                        LabeledContent {
                            TextField("Percentage", value: Binding(
                                get: {
                                    // Get the percentage for this member
                                    payment.percentageFor(memberId: member.friendId) ?? 0.0
                                },
                                set: { newPercentage in
                                    // Convert the percentage back to the amount and update it
                                    payment.updateAmountForPercentage(memberId: member.friendId,
                                                                      percentage: newPercentage)
                                }
                            ), format: .percent.precision(.fractionLength(2)))
                            .multilineTextAlignment(.trailing)
                        } label: {
                            Text(member.friendName)
                        }
                    }
                }

                if isDisabled == false {
                    Section {
                        if payment.amount != payment.calculateGroupMemberTotalAmount(payment: payment, group: group) {
                            Text("""
                                 \(((payment.calculateGroupMemberTotalAmount(
                                 payment: payment, group: group) / payment.amount))
                                 .formatted(.percent.precision(.fractionLength(0)))) does not equal 100%
                                 """)
                            .font(.footnote)
                            .foregroundStyle(.gray)
                        }
                    }
                }

            case .custom:
                ForEach(Array(group.groupMember), id: \.self) { member in
                    HStack {
                        LabeledContent {
                            TextField("Amount", value: Binding(
                                get: {
                                    payment.amountFor(memberId: member.friendId) ?? 0.0
                                },
                                set: { newValue in
                                    payment.updateAmount(for: member.friendId, newAmount: newValue)
                                }
                            ), format: .number)
                            .multilineTextAlignment(.trailing)
                            Text(payment.currency)
                        } label: {
                            Text(member.friendName)
                        }
                    }
                }
            case .none:
                Text("")
            }
        }
        .disabled(isDisabled)
    }
}

#Preview {
    SplitView(payment: .exampleGroup, group: Model.Group.exampleGroup, isDisabled: true)
}
