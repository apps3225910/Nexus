//
//  GroupExpensesDetailView.swift
//  Nexus
//
//  Created by Pascal Hintze on 25.07.2024.
//

import SwiftUI
import CloudKit
import Model

struct GroupExpensesDetailEditView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @Environment(\.dismiss) private var dismiss

    @Binding var showCancelDialog: Bool

    @State private var paidBy: GroupMember?

    @State private var showSumAlert = false
    @State private var showPercentAlert = false
    @State private var showDescriptionAlert = false

    @ObservedObject var group: Model.Group
    @ObservedObject var payment: Payment

    let currencies = Locale.Currency.isoCurrencies

    var body: some View {
        NavigationStack {
            Form {
                HStack {
                    TextField("Description", text: $payment.paymentDescription, prompt: Text("Enter a description"))

                    Picker("Type", selection: $payment.category) {
                        ForEach(PaymentCategory.allCases, id: \.self) { type in
                            Label(type.name, systemImage: type.icon)
                                .labelStyle(.iconOnly)
                        }
                    }
                    .labelsHidden()
                }

                LabeledContent {
                    TextField("Amount", value: $payment.amount, format: .number, prompt: Text("Select amount"))
                        .keyboardType(.decimalPad)
                        .multilineTextAlignment(.trailing)
                } label: {
                    Text("Amount")
                }

                Picker("Currency", selection: $payment.currency) {
                    ForEach(currencies, id: \.self) { currency in
                        Text(currency.identifier).tag(currency.identifier)
                    }
                }

                DatePicker("Date", selection: $payment.entryDate, displayedComponents: [.date])
                    .pickerStyle(.wheel)

                Section("Paid by") {
                    Picker("Paid by", selection: $paidBy) {
                        ForEach(Array(group.groupMember), id: \.self) { member in
                            Text(member.friendName).tag(member as GroupMember?)
                        }
                    }
                }

                Section("Splitted") {
                    Picker("Splitted", selection: $payment.split) {
                        ForEach(PaymentSplit.allCases, id: \.self) { split in
                            Text(split.name)
                        }
                    }
                    .pickerStyle(.segmented)
                }

                SplitView(payment: payment, group: group, isDisabled: false)

            }
            .onAppear {
                paidBy = group.groupMember.first { $0.friendId == payment.paymentOwner }
                viewContext.undoManager?.beginUndoGrouping() // Start an undo group
            }
            .alert("Sum is Not Adding Up", isPresented: $showSumAlert) {
                Button("OK") { }
            } message: {
                Text("The splitted sum is not adding up to the total of the payment.")
            }

            .alert("Percentages are Not Adding Up", isPresented: $showPercentAlert) {
                Button("OK") { }
            } message: {
                Text("The percentages are not adding up to 100%.")
            }

            .alert("Please Enter a Description", isPresented: $showDescriptionAlert) {
                Button("OK") { }
            }
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    Button("Cancel") {
                        hideKeyboard()
                        showCancelDialog = true
                    }
                    .confirmationDialog("Are you sure you want to discard your changes?",
                                        isPresented: $showCancelDialog, titleVisibility: .visible) {

                        Button("Discard Changes", role: .destructive) {
                            viewContext.undoManager?.endUndoGrouping() // End the undo group
                            viewContext.undoManager?.undo() // Undo all changes in this group
                            viewContext.refresh(payment, mergeChanges: false)
                            dismiss()
                        }

                        Button("Keep editing", role: .cancel) { }
                    }
                }
                ToolbarItem(placement: .confirmationAction) {
                    Button("Done") {
                        viewContext.undoManager?.endUndoGrouping() // End the undo group
                        Task {
                            if payment.paymentDescription.isEmpty {
                                showDescriptionAlert.toggle()
                            } else if payment.amount != payment.calculateGroupMemberTotalAmount(
                                payment: payment, group: group) {
                                if PaymentSplit(rawValue: payment.paymentSplit) == .equally ||
                                    PaymentSplit(rawValue: payment.paymentSplit) == .custom {
                                    showSumAlert.toggle()
                                }
                                if PaymentSplit(rawValue: payment.paymentSplit)  == .percent {
                                    showPercentAlert.toggle()
                                }
                            } else {
                                if let owner = paidBy {
                                    payment.paymentOwner = owner.friendId
                                    payment.paymentOwnerName = owner.friendName
                                }

                                try viewContext.save()

                                dismiss()
                            }
                        }
                    }
                }
            }
            .navigationTitle(payment.paymentDescription)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#Preview {
    GroupExpensesDetailView(group: .exampleGroup, payment: .exampleGroup)
}
