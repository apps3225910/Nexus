//
//  PhotoPicker.swift
//  Nexus
//
//  Created by Pascal Hintze on 07.09.2024.
//

import SwiftUI
import PhotosUI
import UIKit

struct PhotoPicker: View {
    @Binding var userItem: PhotosPickerItem?
    @Binding var userImage: Data?

    let text: String

    var body: some View {
        VStack {
            PhotosPicker(selection: $userItem, matching: .images) {
                VStack {
                    if let image = userImage {
                        Image(uiImage: UIImage(data: image) ?? UIImage())
                            .resizable()
                            .scaledToFill()
                            .frame(width: 72, height: 72)
                            .clipShape(Circle())
                    } else {
                        Image(systemName: "person")
                            .font(.title)
                            .fontWeight(.semibold)
                            .foregroundStyle(.white)
                            .frame(width: 72, height: 72)
                            .background(Color(.systemGray3))
                            .clipShape(Circle())
                    }
                }
            }

            Text(text)
                .font(.footnote)
        }
        .onChange(of: userItem) {
            Task {
                if let loadedData = try? await userItem?.loadTransferable(type: Data.self),
                   let image = UIImage(data: loadedData) {
                    userImage = image.jpegData(compressionQuality: 1.0)
                }

            }
        }
    }
}

#Preview {
    PhotoPicker(userItem: .constant(nil), userImage: .constant(nil), text: "Select your photo")
}
