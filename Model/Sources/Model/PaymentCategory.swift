//
//  PaymentType.swift
//  Nexus
//
//  Created by Pascal Hintze on 13.04.2024.
//

import Foundation
import SwiftUI

@objc public enum PaymentCategory: Int16, CaseIterable, Identifiable {
    case accomodation
    case diningOut
    case drinks
    case entertainment
    case general
    case groceries
    case transportation

    public var id: Int16 { self.rawValue }

    public var name: String {
        switch self {
        case .accomodation:
            return "Accomodation"
        case .diningOut:
            return "Dining out"
        case .drinks:
            return "Drinks"
        case .entertainment:
            return "Entertainment"
        case .general:
            return "General"
        case .groceries:
            return "Groceries"
        case .transportation:
            return "Transportation"
        }
    }

    public var icon: String {
        switch self {
        case .accomodation:
            "bed.double"
        case .diningOut:
            "takeoutbag.and.cup.and.straw"
        case .drinks:
            "wineglass"
        case .entertainment:
            "theatermasks"
        case .general:
            "list.bullet.clipboard"
        case .groceries:
            "bag"
        case .transportation:
            "car"
        }
    }

    public var color: Color {
        switch self {
        case .accomodation:
                .teal
        case .diningOut:
                .orange
        case .drinks:
                .purple
        case .entertainment:
                .red
        case .general:
                .gray
        case .groceries:
                .green
        case .transportation:
                .blue
        }
    }
}
