//
//  File.swift
//  Model
//
//  Created by Pascal Hintze on 29.10.2024.
//

import Foundation

public struct MemberBalance: Equatable {
    public let otherMember: String
    public let otherMemberPicture: Data
    public let otherMemberName: String
    public let amountOwedOrDue: Double

    public init(otherMember: String, otherMemberPicture: Data, otherMemberName: String, amountOwedOrDue: Double) {
        self.otherMember = otherMember
        self.otherMemberPicture = otherMemberPicture
        self.otherMemberName = otherMemberName
        self.amountOwedOrDue = amountOwedOrDue
    }
}
