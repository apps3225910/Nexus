//
//  File.swift
//  
//
//  Created by Pascal Hintze on 03.09.2024.
//

import Foundation

public enum FilterOptions: String, CaseIterable, Identifiable {
     case all = "All"
     case open = "Open"
     case closed = "Settled"

     public var id: String { self.rawValue }

    public var icon: String? {
        switch self {
        case .all:
            nil
        case .open:
            "book"
        case .closed:
            "book.closed"
        }
    }
 }
