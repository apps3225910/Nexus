//
//  PersistenceController+UISharingController.swift
//
//
//  Created by Pascal Hintze on 01.07.2024.
//

import UIKit
import CloudKit

/**
 Create and present a UICloudSharingController object for CloudKit sharing management.
 This sample uses ShareLink to create a new share,
 and  uses UICloudSharingController to manage an existing share in iOS.
 */
extension PersistenceController {
  public func presentCloudSharingController(share: CKShare) {
        let sharingController = UICloudSharingController(share: share, container: cloudKitContainer)

//                sharingController.delegate = self
        /**
         Setting the presentation style to .formSheet
         so there's no need to specify sourceView, sourceItem, or sourceRect.
         */
        if let viewController = rootViewController {
            sharingController.modalPresentationStyle = .formSheet
//            sharingController.availablePermissions = [.allowReadWrite, .allowPrivate]
//            sharingController.availablePermissions = [.allowReadWrite, .allowPublic]
            viewController.present(sharingController, animated: true)
        }
    }

    private var rootViewController: UIViewController? {
        for scene in UIApplication.shared.connectedScenes {
            if scene.activationState == .foregroundActive,
               let sceneDeleate = (scene as? UIWindowScene)?.delegate as? UIWindowSceneDelegate,
               let window = sceneDeleate.window {
                return window?.rootViewController
            }
        }
        print("\(#function): Failed to retrieve the window's root view controller.")
        return nil
    }
}
