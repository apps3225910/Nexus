//
//  PersistenceController+User.swift
//  Nexus
//
//  Created by Pascal Hintze on 12.06.2024.
//

import Foundation
import CoreData
import CloudKit
import UIKit

extension PersistenceController {
    public func addUser(username: String, iCloudID: String, profilepicture: Data?,
                        context: NSManagedObjectContext) async {
        await context.perform {
            let user = User(context: context)
            context.assign(user, to: self.privatePersistentStore)
            user.ckID = iCloudID
            user.username = username

            if let profilepicture {
                        user.profilepicture = profilepicture
            } else {
                if let defaultImage = UIImage(named: "house"),
                let imageData = defaultImage.pngData() {
                    user.profilepicture = imageData
                }
            }
            try? context.save()
        }
    }

//    public func getUserInfo(userId: String) async -> (name: String, iCloudId: String) {
//        let predicate = NSPredicate(format: "CD_ckID == %@", userId)
//        let query = CKQuery(recordType: "CD_User", predicate: predicate)
//        let publicDB = CKContainer.default().publicCloudDatabase
//
//        var friendResult: (String, String) = ("", "")
//
//        do {
//            let (matchResults, _) = try await withCheckedThrowingContinuation { continuation in
//                publicDB.fetch(withQuery: query) { result in
//                    continuation.resume(with: result)
//                }
//            }
//            for matchResult in matchResults {
//                switch matchResult.1 {
//                case .success(let record):
//                    if let name = record.object(forKey: "CD_username") as? String {
//                        if let iCloudID = record.object(forKey: "CD_ckID") as? String {
//
//                            friendResult = (name, iCloudID)
//                        }
//                    }
//                case .failure(let error):
//                    print("Error fetching user record: \(error.localizedDescription)")
//                }
//            }
//        } catch {
//            print("Error performing query: \(error.localizedDescription)")
//        }
//        return friendResult
//    }
}
