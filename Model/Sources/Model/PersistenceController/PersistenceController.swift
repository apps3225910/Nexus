//
//  PersistenceController.swift
//  Nexus
//
//  Created by Pascal Hintze on 06.04.2024.
//

import CoreData
import Foundation
import CloudKit

let gCloudKitContainerIdentifier = "iCloud.com.example.FairShare"

/**
 This app doesn't necessarily post notifications from the main queue.
 */
extension Notification.Name {
    public static let cdcksStoreDidChange = Notification.Name("cdcksStoreDidChange")
}

struct UserInfoKey {
    static let storeUUID = "storeUUID"
    static let transactions = "transactions"
}

public struct TransactionAuthor {
    public static let app = "app"
}

public class PersistenceController: ObservableObject {
    public static let shared = PersistenceController()

    static let modelURL = Bundle.module.url(forResource: "GroupsDataModel", withExtension: "momd")!

    static let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)!

    public let persistentContainer: NSPersistentCloudKitContainer

    public init(inMemory: Bool = false) {
        persistentContainer = NSPersistentCloudKitContainer(
            name: "GroupsDataModel",
            managedObjectModel: PersistenceController.managedObjectModel)

        if inMemory {
            persistentContainer.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }

        let baseURL = NSPersistentContainer.defaultDirectoryURL()
        let storeFolderURL = baseURL.appendingPathComponent("CoreDataStores")
        let privateStoreFolderURL = storeFolderURL.appendingPathComponent("Private")
        let sharedStoreFolderURL = storeFolderURL.appendingPathComponent("Shared")
        let publicStoreFolderURL = storeFolderURL.appendingPathComponent("Public")

        let fileManager = FileManager.default
        for folderURL in [privateStoreFolderURL, sharedStoreFolderURL, publicStoreFolderURL] where
        !fileManager.fileExists(
            atPath: folderURL.path) {
                do {
                    try fileManager.createDirectory(at: folderURL, withIntermediateDirectories: true,
                                                    attributes: nil)
                } catch {
                    fatalError("#\(#function): Failed to create the store folder: \(error)")
                }
            }

        persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        persistentContainer.viewContext.automaticallyMergesChangesFromParent = true

        // Setup of the private store

        guard let privateStoreDescription = persistentContainer.persistentStoreDescriptions.first else {
            fatalError("Failed to initialize persistent container")
        }

//        deleteStore(for: privateStoreDescription)

        setupPrivateStore(for: privateStoreDescription, with: privateStoreFolderURL)

        // Setup of the shared store

        guard let sharedStoreDescription = privateStoreDescription.copy() as? NSPersistentStoreDescription else {
            fatalError("#\(#function): Copying the private store description returned an unexpected value.")
        }

//        deleteStore(for: sharedStoreDescription)

        setupSharedStore(for: sharedStoreDescription, with: sharedStoreFolderURL)

        // Setup of the public store

        guard let publicStoreDescription = privateStoreDescription.copy() as? NSPersistentStoreDescription else {
            fatalError("#\(#function): Copying the private store description returned an unexpected value.")
        }

        setupPublicStore(for: publicStoreDescription, with: publicStoreFolderURL)

        // Load the persistent stores

        persistentContainer.persistentStoreDescriptions.append(sharedStoreDescription)
        persistentContainer.persistentStoreDescriptions.append(publicStoreDescription)
        persistentContainer.loadPersistentStores(completionHandler: { (loadedStoreDescription, error) in
            guard error == nil else {
                fatalError("#\(#function): Failed to load persistent stores:\(error!)")
            }
            guard let cloudKitContainerOptions = loadedStoreDescription.cloudKitContainerOptions else {
                return
            }
            if cloudKitContainerOptions.databaseScope == .private {
                self._privatePersistentStore = self.persistentContainer.persistentStoreCoordinator.persistentStore(
                    for: loadedStoreDescription.url!)
            } else if cloudKitContainerOptions.databaseScope  == .shared {
                self._sharedPersistentStore = self.persistentContainer.persistentStoreCoordinator.persistentStore(
                    for: loadedStoreDescription.url!)
            } else if cloudKitContainerOptions.databaseScope == .public {
                self._publicPersistentStore = self.persistentContainer.persistentStoreCoordinator.persistentStore(
                    for: loadedStoreDescription.url!)
            }
        })

        /**
         Run initializeCloudKitSchema() once to update the CloudKit schema every time you change the Core Data model.
         Dispatch to the next runloop in the main queue to avoid blocking AppKit app life-cycle delegate methods.
         Don't call this code in the production environment.
         */
#if InitializeCloudKitSchema
        DispatchQueue.main.async {
            do {
                try container.initializeCloudKitSchema()
            } catch {
                print("\(#function): initializeCloudKitSchema: \(error)")
            }
        }
#else
        persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        persistentContainer.viewContext.transactionAuthor = TransactionAuthor.app

        /**
         Automatically merge the changes from other contexts.
         */
        persistentContainer.viewContext.automaticallyMergesChangesFromParent = true

        if !inMemory {
            /**
             Pin the viewContext to the current generation token and set it to keep itself up-to-date with local changes.
             */
            do {
                try persistentContainer.viewContext.setQueryGenerationFrom(.current)
            } catch {
                fatalError("#\(#function): Failed to pin viewContext to the current generation:\(error)")
            }
        }
                /**
                 Observe the following notifications:
                 - The remote change notifications from container.persistentStoreCoordinator.
                 - The event change notifications from the container.
                 */
                NotificationCenter.default.addObserver(self, selector: #selector(storeRemoteChange(_:)),
                                                       name: .NSPersistentStoreRemoteChange,
                                                       object: persistentContainer.persistentStoreCoordinator)
//                NotificationCenter.default.addObserver(self, selector: #selector(containerEventChanged(_:)),
//                                                       name: NSPersistentCloudKitContainer.eventChangedNotification,
//                                                       object: container)
#endif

    }

    private func setupPrivateStore(for storeDescription: NSPersistentStoreDescription, with storeFolderURL: URL) {
        storeDescription.url = storeFolderURL.appendingPathComponent("private.sqlite")

        storeDescription.setOption(true as NSNumber, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
        storeDescription.setOption(true as NSNumber, forKey: NSPersistentHistoryTrackingKey)

        let cloudKitContainerOptions = NSPersistentCloudKitContainerOptions(
            containerIdentifier: gCloudKitContainerIdentifier)
        cloudKitContainerOptions.databaseScope = .private
        storeDescription.cloudKitContainerOptions = cloudKitContainerOptions
    }

    private func setupSharedStore(for storeDescription: NSPersistentStoreDescription, with storeFolderURL: URL) {
        storeDescription.url = storeFolderURL.appendingPathComponent("shared.sqlite")

        storeDescription.setOption(true as NSNumber, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)
        storeDescription.setOption(true as NSNumber, forKey: NSPersistentHistoryTrackingKey)

        let sharedStoreOptions = NSPersistentCloudKitContainerOptions(
            containerIdentifier: gCloudKitContainerIdentifier)
        sharedStoreOptions.databaseScope = .shared
        storeDescription.cloudKitContainerOptions = sharedStoreOptions
    }

    private func setupPublicStore(for storeDescription: NSPersistentStoreDescription, with storeFolderURL: URL) {
        storeDescription.url = storeFolderURL.appendingPathComponent("public.sqlite")

        let publicStoreOptions = NSPersistentCloudKitContainerOptions(
            containerIdentifier: gCloudKitContainerIdentifier)
        publicStoreOptions.databaseScope = .public
        storeDescription.cloudKitContainerOptions = publicStoreOptions
    }

    private func deleteStore(for storeDescription: NSPersistentStoreDescription) {
        //         Deltes the respective store
                if let storeURL = storeDescription.url {
                    do {
                        try persistentContainer.persistentStoreCoordinator.destroyPersistentStore(
                            at: storeURL, ofType: NSSQLiteStoreType, options: nil)
                        print("Persistent store removed successfully.")
                        UserDefaults.standard.removeObject(forKey: "lastHistoryToken")
                    } catch {
                        print("Failed to remove persistent store: \(error)")
                    }
                }
    }

    private var _privatePersistentStore: NSPersistentStore?
    public var privatePersistentStore: NSPersistentStore {
        return _privatePersistentStore!
    }

    private var _sharedPersistentStore: NSPersistentStore?
    public var sharedPersistentStore: NSPersistentStore {
        return _sharedPersistentStore!
    }

    private var _publicPersistentStore: NSPersistentStore?
    var publicPersistentStore: NSPersistentStore {
        return _publicPersistentStore!
    }

    public lazy var cloudKitContainer: CKContainer = {
        return CKContainer(identifier: gCloudKitContainerIdentifier)
    }()

    /**
     An operation queue for handling history-processing tasks: watching changes, deduplicating tags, and triggering UI updates, if needed.
     */
    public lazy var historyQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}

extension PersistenceController {
    public func save() {
        let context = PersistenceController.shared.persistentContainer.viewContext

        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                // Show some error here
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }

}

extension NSPersistentCloudKitContainer {
    /**
     A convenience method for creating background contexts that specify the app as their transaction author.
     */
    func newTaskContext() -> NSManagedObjectContext {
        let context = newBackgroundContext()
        context.transactionAuthor = TransactionAuthor.app
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }
}
