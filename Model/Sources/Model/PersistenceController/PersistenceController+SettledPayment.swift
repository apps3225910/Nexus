//
//  Untitled.swift
//  Model
//
//  Created by Pascal Hintze on 20.01.2025.
//

import Foundation
import CoreData

extension PersistenceController {
    public func fetchSettlementsForGroup(_ group: Model.Group) -> [SettledPayment] {
        let context = PersistenceController.shared.persistentContainer.viewContext

        // Fetch SettledPayments for the given group
        let settledPaymentFetchRequest: NSFetchRequest<SettledPayment> = SettledPayment.fetchRequest()
        settledPaymentFetchRequest.predicate = NSPredicate(format: "group == %@", group)
        settledPaymentFetchRequest.predicate = NSPredicate(format: "isSettledPaymentDeleted == false")

        do {
            let settlements = try context.fetch(settledPaymentFetchRequest)
            return settlements
        } catch let error as NSError {
            print("Could not fetch SettledPayments. \(error), \(error.userInfo)")
            return []
        }
    }

    public func fetchGroupMembersWithBalance(in group: Model.Group) -> [String: Double] {
        let context = PersistenceController.shared.persistentContainer.viewContext

        // Step 1: Fetch all Payments for the Group
        let paymentFetchRequest: NSFetchRequest<Payment> = Payment.fetchRequest()
        paymentFetchRequest.predicate = NSPredicate(format: "groupId == %@", group.id as CVarArg)

        do {
            let payments = try context.fetch(paymentFetchRequest)

            // Dictionary to store the balance for each GroupMember by their friendId
            var balanceDict: [String: Double] = [:]

            // Initialize balance for each member
            for member in group.groupMember {
                balanceDict[member.friendId] = 0.0
            }

            for payment in payments {
                // Step 2: Fetch PaymentAmounts related to this payment
                let paymentAmountFetchRequest: NSFetchRequest<PaymentAmount> = PaymentAmount.fetchRequest()
                paymentAmountFetchRequest.predicate = NSPredicate(format: "payment == %@", payment)

                let paymentAmounts = try context.fetch(paymentAmountFetchRequest)

                // Calculate total amount owed in this payment
                let totalAmount = payment.amount

                // Split amount equally among the members involved in the payment
                let splitAmount = totalAmount / Double(paymentAmounts.count)

                for paymentAmount in paymentAmounts {
                    let memberId = paymentAmount.memberId

                    // Check if this member is the payment owner
                    if payment.paymentOwner == memberId {
                        // This member is the owner, so they will receive amounts owed to them
                        balanceDict[memberId, default: 0.0] += splitAmount * Double(paymentAmounts.count - 1)
                        // They are owed by other members
                    } else {
                        // This member owes the payment owner their share
                        balanceDict[memberId, default: 0.0] -= splitAmount  // They owe the owner
                    }
                }
            }

            return balanceDict

        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return [:]
        }
    }

    public func deleteFlaggedSettledPayments(context: NSManagedObjectContext) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = SettledPayment.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isSettledPaymentDeleted == true")

        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(batchDeleteRequest)
            print("Deleted flagged settled payments successfully")
        } catch {
            print("Failed to delete flagged settled payments: \(error)")
        }
    }
}
