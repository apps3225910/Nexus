//
//  File.swift
//  
//
//  Created by Pascal Hintze on 19.08.2024.
//

import Foundation
import CloudKit
import CoreData

extension PersistenceController {
    public func shareObject(_ unsharedObject: NSManagedObject, to existingShare: CKShare?,
                            completionHandler: ((_ share: CKShare?, _ error: Error?) -> Void)? = nil) {
        persistentContainer.share([unsharedObject], to: existingShare) { (_, share, _, error) in
            guard error == nil, let share = share else {
                print("\(#function): Failed to share an object: \(error!))")
                completionHandler?(share, error)
                return
            }
            /**
             Synchronize the changes on the share to the private persistent store.
             */
            if share.recordID.zoneID.ownerName == "__defaultOwner__" {
                self.persistentContainer.persistUpdatedShare(share, in: self.privatePersistentStore) { (share, error) in
                    if let error = error {
                        print("\(#function): Failed to persist updated share: \(error)")
                    }
                    completionHandler?(share, error)
                }
            } else {
                self.persistentContainer.persistUpdatedShare(share, in: self.sharedPersistentStore) { (share, error) in
                    if let error = error {
                        print("\(#function): Failed to persist updated share: \(error)")
                    }
                    completionHandler?(share, error)
                }
            }
        }
    }

    public func existingShare(objectID: NSManagedObjectID) -> CKShare? {
        if let shareSet = try? persistentContainer.fetchShares(matching: [objectID]),
           let (_, share) = shareSet.first {
            return share
        }
        return nil
    }
}
