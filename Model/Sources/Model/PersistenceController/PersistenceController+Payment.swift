//
//  PersistenceController+Payment.swift
//
//
//  Created by Pascal Hintze on 24.06.2024.
//

import Foundation
import CoreData
import CloudKit
import SwiftUI

extension PersistenceController {
    // swiftlint:disable:next function_parameter_count
    public func createFriendPayment(amount: Double,
                                    currency: String,
                                    category: PaymentCategory,
                                    description: String,
                                    paidBy: String,
                                    paymentSplit: PaymentSplit,
                                    friendAmount: Double,
                                    userAmount: Double,
                                    friendPercent: Double,
                                    userPercent: Double,
                                    group: Model.Group,
                                    groupMember: GroupMember,
                                    user: GroupMember,
                                    context: NSManagedObjectContext
    ) async -> Payment {

        await context.perform {
            let newPayment = Payment(context: context)
            newPayment.amount = amount
            newPayment.currency = currency
            newPayment.entryDate = .now
            newPayment.id = UUID()
            newPayment.paymentDescription = description
            newPayment.paymentCategory = category.rawValue
            newPayment.paymentOwner = paidBy
            newPayment.groupId = group.id.uuidString
            newPayment.group = group
            newPayment.isPaymentDeleted = false

            if paymentSplit == .percent {
                newPayment.friendAmount = amount * (friendPercent / 100)
                newPayment.userAmount = amount * (userPercent / 100)
            } else {
                newPayment.friendAmount = friendAmount
                newPayment.userAmount = userAmount
            }

            user.amount += userAmount
            groupMember.amount += friendAmount

            try? context.save()

            return newPayment
        }
    }

    public func deleteFlaggedPayments(context: NSManagedObjectContext) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Payment.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isPaymentDeleted == true")

        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(batchDeleteRequest)
            print("Deleted flagged payments successfully")
        } catch {
            print("Failed to delete flagged payments: \(error)")
        }
    }
}
