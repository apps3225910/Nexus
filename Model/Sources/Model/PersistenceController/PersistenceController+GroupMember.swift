//
//  File.swift
//  Model
//
//  Created by Pascal Hintze on 27.01.2025.
//

import Foundation

extension PersistenceController {
    public func fetchMemberDetails(for member: Model.GroupMember, in group: Model.Group) -> [(
        otherMemberId: String, amount: Double)] {

            var details: [(otherMemberId: String, amount: Double)] = []

            // Fetch balances based on payments first
            let calculatedBalances = PersistenceController.shared.fetchGroupMembersWithBalance(in: group)

            // Initialize the details list with the calculated balances
            var balanceDict: [String: Double] = [:]
            for (memberId, balance) in calculatedBalances {
                balanceDict[memberId] = balance
            }

            // Fetch and apply settlements (settlements that involve the selected member)
            let settlements = PersistenceController.shared.fetchSettlementsForGroup(group)

            // Apply settlements to adjust the balances correctly
            for settlement in settlements {
                balanceDict[settlement.payer, default: 0.0] += abs(settlement.amount)
                balanceDict[settlement.payee, default: 0.0] -= abs(settlement.amount)
            }

            // Now create the details list based on the updated balances
            for (memberId, balance) in balanceDict {
                if let otherMember = group.groupMember.first(where: { $0.friendId == memberId }) {
                    if otherMember.friendId != member.friendId { // Exclude the current member
                        details.append((otherMemberId: memberId, amount: balance))
                    }
                }
            }

            // Return only amounts != 0
            return details.filter { $0.amount != 0.0 }
        }

    // Function to calculate balances
    public func fetchGroupMemberBalances(in group: Model.Group) -> [String: Double] {
        var balances: [String: Double] = [:]

        let calculatedBalances = PersistenceController.shared.fetchGroupMembersWithBalance(in: group)

        for (memberId, balance) in calculatedBalances {
            if let member = group.groupMember.first(where: { $0.friendId == memberId }) {
                balances[member.friendId] = balance
            }
        }

        // Adjust balances with settled payments
        let settlements = PersistenceController.shared.fetchSettlementsForGroup(group)

        for settlement in settlements {
            let settlementAmount = abs(settlement.amount)
            // Reduce the amount owed by the payer
            balances[settlement.payer, default: 0.0] += settlementAmount
            // Reduce the amount credited to the payee
            balances[settlement.payee, default: 0.0] -= settlementAmount
        }
        return balances
    }
}
