//
//  File.swift
//  Model
//
//  Created by Pascal Hintze on 02.03.2025.
//

import CoreData
import CloudKit

// MARK: - Notification handlers that trigger history processing.
//
extension PersistenceController {
    /**
     Handle .NSPersistentStoreRemoteChange notifications.
     Process persistent history to merge relevant changes to the context, and deduplicate the tags, if necessary.
     */
    @objc
    func storeRemoteChange(_ notification: Notification) {
        guard let storeUUID = notification.userInfo?[NSStoreUUIDKey] as? String,
              [privatePersistentStore.identifier, sharedPersistentStore.identifier,
               publicPersistentStore.identifier].contains(storeUUID) else {
            print("\(#function): Ignore a store remote Change notification because of no valid storeUUID.")
            print("storeUUID: \(String(describing: notification.userInfo?[NSStoreUUIDKey]))")
            return
        }
        processHistoryAsynchronously(storeUUID: storeUUID)
    }
}

// MARK: - Process persistent history asynchronously.
//
extension PersistenceController {
    /**
     Process persistent history, posting any relevant transactions to the current view.
     This method processes the new history since the last history token, and is simply a fetch if there's no new history.
     */
    private func processHistoryAsynchronously(storeUUID: String) {
        historyQueue.addOperation {
//            let context = self.persistentContainer.newBackgroundContext()
            let taskContext = self.persistentContainer.newTaskContext()
            taskContext.performAndWait {
                self.performHistoryProcessing(storeUUID: storeUUID, performingContext: taskContext)
            }
        }
    }

    private func performHistoryProcessing(storeUUID: String, performingContext: NSManagedObjectContext) {
        /**
         Fetch the history by the other author since the last timestamp.
        */
        let lastHistoryToken = historyToken(with: storeUUID)
        let request = NSPersistentHistoryChangeRequest.fetchHistory(after: lastHistoryToken)
        let historyFetchRequest = NSPersistentHistoryTransaction.fetchRequest!
        historyFetchRequest.predicate = NSPredicate(format: "author != %@", TransactionAuthor.app)
        request.fetchRequest = historyFetchRequest

        if privatePersistentStore.identifier == storeUUID {
            request.affectedStores = [privatePersistentStore]
        } else if sharedPersistentStore.identifier == storeUUID {
            request.affectedStores = [sharedPersistentStore]
        }

        let result = (try? performingContext.execute(request)) as? NSPersistentHistoryResult
        guard let transactions = result?.result as? [NSPersistentHistoryTransaction] else {
            return
        }
         print("\(#function): Processing transactions: \(transactions.count).")

        /**
         Post transactions so observers can update the UI, even when transactions is empty
         because when a share changes, Core Data triggers a store remote change notification with no transaction.
         */
        let userInfo: [String: Any] = [UserInfoKey.storeUUID: storeUUID, UserInfoKey.transactions: transactions]
        NotificationCenter.default.post(name: .cdcksStoreDidChange, object: self, userInfo: userInfo)
        /**
         Update the history token using the last transaction. The last transaction has the latest token.
         */
        if let newToken = transactions.last?.token {
            updateHistoryToken(with: storeUUID, newToken: newToken)
        }

        var newGroupsObjectIDs = [NSManagedObjectID]()
        let groupEntityName = Group.entity().name

        for transaction in transactions where transaction.changes != nil {
            for change in transaction.changes! {
                if change.changedObjectID.entity.name == groupEntityName && change.changeType == .insert {
                    newGroupsObjectIDs.append(change.changedObjectID)
                }
            }
        }

        if !newGroupsObjectIDs.isEmpty {
            addUserAsGroupMemberAndWait(groupObjectIDs: newGroupsObjectIDs)
        }
    }

    private func addUserAsGroupMemberAndWait(groupObjectIDs: [NSManagedObjectID]) {
        let taskContext = PersistenceController.shared.persistentContainer.newTaskContext()
        taskContext.performAndWait {
            groupObjectIDs.forEach { groupObjectID in
                addUserAsGroupMember(groupObjectID: groupObjectID, performingContext: taskContext)
            }
        }
    }

    private func addUserAsGroupMember(groupObjectID: NSManagedObjectID, performingContext: NSManagedObjectContext) {
        guard let group = performingContext.object(with: groupObjectID) as? Group else {
            print("\(#function): Could not find group with ID \(groupObjectID)")
            return
        }

        // Find the current user in Core Data
        let fetchRequest: NSFetchRequest<User> = User.fetchRequest()
        fetchRequest.predicate = NSPredicate(value: true)

        guard let user = try? performingContext.fetch(fetchRequest).first else {
            print("\(#function): Could not fetch user")
            return
        }

        if group.groupMember.contains(where: { $0.friendId == user.ckID }) {
            print(
                "User \(user.username) is already a member of group \(group.id.uuidString). Skipping...")
            return
        }

        let groupMember = GroupMember(context: performingContext)
        groupMember.id = UUID()
        groupMember.friendName = user.username
        groupMember.friendId = user.ckID
        groupMember.amount = 0
        groupMember.groupId = group.id.uuidString
        groupMember.group = group
        groupMember.picture = user.profilepicture

        performingContext.assign(groupMember, to: PersistenceController.shared.sharedPersistentStore)

        try? performingContext.save()
    }

    /**
     Track the last history tokens for the stores.
     The historyQueue reads the token when executing operations, and updates it after completing the processing.
     Access this user default from the history queue.
     */
    private func historyToken(with storeUUID: String) -> NSPersistentHistoryToken? {
        let key = "HistoryToken" + storeUUID
        if let data = UserDefaults.standard.data(forKey: key) {
            return  try? NSKeyedUnarchiver.unarchivedObject(ofClass: NSPersistentHistoryToken.self, from: data)
        }
        return nil
    }

    private func updateHistoryToken(with storeUUID: String, newToken: NSPersistentHistoryToken) {
        let key = "HistoryToken" + storeUUID
        let data = try? NSKeyedArchiver.archivedData(withRootObject: newToken, requiringSecureCoding: true)
        UserDefaults.standard.set(data, forKey: key)
    }
}
