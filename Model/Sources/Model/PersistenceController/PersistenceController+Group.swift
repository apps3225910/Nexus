//
//  PersistenceController+Group.swift
//
//
//  Created by Pascal Hintze on 19.07.2024.
//

import CoreData
import CloudKit
import SwiftUI

extension PersistenceController {
    public func addGroup(name: String, type: GroupTypes, owner: GroupMember,
                         groupColor: GroupColors,
                         context: NSManagedObjectContext) async {
        await context.perform {
            let group = Group(context: context)
            context.assign(group, to: self.privatePersistentStore)
            group.id = UUID()
            group.name = name
            group.openAmount = 0
            group.type = type.rawValue
            group.groupColor = groupColor
            group.owner = owner.friendId
            group.isGroupDeleted = false

            let groupMember = GroupMember(context: context)
            context.assign(groupMember, to: self.privatePersistentStore)
            groupMember.friendName = owner.friendName
            groupMember.amount = owner.amount
            groupMember.picture = owner.picture
            groupMember.friendId = owner.friendId
            groupMember.groupId = group.id.uuidString
            groupMember.group = group

            try? context.save()
        }
    }

    public func deleteFlaggedGroups(context: NSManagedObjectContext) {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Group.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "isGroupDeleted == true")

        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(batchDeleteRequest)
            print("Deleted flagged groups successfully")
        } catch {
            print("Failed to delete flagged groups: \(error)")
        }
    }
}

extension Group: Transferable {
    public static var transferRepresentation: some TransferRepresentation {
        CKShareTransferRepresentation { groupToExport in
            let persistentContainer = PersistenceController.shared.persistentContainer
            let ckContainer = PersistenceController.shared.cloudKitContainer

            var groupShare: CKShare?
            if let shareSet = try? persistentContainer.fetchShares(matching: [groupToExport.objectID]),
               let (_, share) = shareSet.first {
                groupShare = share
            }
            /**
             Return the existing share if the group already has a share.
             */
            if let share = groupShare {
                groupToExport.managedObjectContext?.performAndWait {
                    share[CKShare.SystemFieldKey.title] = "You're Invited to Join \(groupToExport.name)!"
                }
                return .existing(share, container: ckContainer)
            }
            /**
             Otherwise, create a new share for the group and return it.
             Use uriRepresentation of the object in the Sendable closure.
             */
            let groupURI = groupToExport.objectID.uriRepresentation()
            return .prepareShare(container: ckContainer) {
                let persistentContainer = PersistenceController.shared.persistentContainer
                let group = await persistentContainer.viewContext.perform {
                    let coordinator = persistentContainer.viewContext.persistentStoreCoordinator
                    guard let objectID = coordinator?.managedObjectID(forURIRepresentation: groupURI) else {
                        fatalError("Failed to return the managed objectID for: \(groupURI).")
                    }
                    return persistentContainer.viewContext.object(with: objectID)
                }
                let (_, share, _) = try await persistentContainer.share([group], to: nil)

                await persistentContainer.viewContext.perform {

                    // Cast the group object to your model type `Group`
                    guard let groupObject = group as? Model.Group else {
                        fatalError("Failed to cast NSManagedObject to Group")
                    }

                    share[CKShare.SystemFieldKey.title] = "You're Invited to Join \(groupObject.name)!"
                }
                return share
            }
        }
    }
}

extension PersistenceController {
    public func groupTransactions(from notification: Notification) -> [NSPersistentHistoryTransaction] {
        var results = [NSPersistentHistoryTransaction]()
        if let transactions = notification.userInfo?[UserInfoKey.transactions] as? [NSPersistentHistoryTransaction] {
            let groupEntityName = Group.entity().name
            for transaction in transactions where transaction.changes != nil {
                for change in transaction.changes! where change.changedObjectID.entity.name == groupEntityName {
                    results.append(transaction)
                    break // Jump to the next transaction.
                }
            }
        }
        return results
    }

   public func mergeTransactions(_ transactions: [NSPersistentHistoryTransaction], to context: NSManagedObjectContext) {
        context.perform {
            for transaction in transactions {
                context.mergeChanges(fromContextDidSave: transaction.objectIDNotification())
            }
        }
    }
}
