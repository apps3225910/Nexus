//
//  PersistenceController+GroupPayment.swift
//
//
//  Created by Pascal Hintze on 29.07.2024.
//

import CloudKit
import CoreData
import Foundation

extension PersistenceController {
    public func createGroupPayment(expenseInfo: ExpenseInfo,
                                   context: NSManagedObjectContext
    ) async {

        await context.perform {
            let newPayment = Payment(context: context)
            newPayment.amount = expenseInfo.amount
            newPayment.currency = expenseInfo.currency
            newPayment.entryDate = expenseInfo.date
            newPayment.paymentCategory = expenseInfo.category.rawValue
            newPayment.id = UUID()
            newPayment.paymentDescription = expenseInfo.description
            newPayment.paymentOwner = expenseInfo.paidBy.friendId
            newPayment.paymentOwnerName = expenseInfo.paidBy.friendName
            newPayment.groupId = expenseInfo.group.id.uuidString
            newPayment.paymentSplit = expenseInfo.paymentSplit.rawValue
            newPayment.group = expenseInfo.group

            for entry in expenseInfo.amounts {
                let newAmountEntry = PaymentAmount(context: context)
                newAmountEntry.amount = entry.value
                newAmountEntry.openAmount = entry.value
                newAmountEntry.memberId = entry.key
                newPayment.addToPaymentAmount(newAmountEntry)
            }

            expenseInfo.group.openAmount += expenseInfo.amount

            for member in expenseInfo.group.groupMember {
                self.addGroupMemberAmounts(member: member, amounts: expenseInfo.amounts)
            }

            try? context.save()
        }
    }

    private func addGroupMemberAmounts(member: GroupMember, amounts: [String: Double]) {
        member.amount += amounts[member.friendId] ?? 0
    }

    // create new settled payment record and add it to the group for the groups payment history
    public func createSettledPayment(
        paymentInfo: SettledPaymentInfo,
        group: Model.Group,
        context: NSManagedObjectContext
    ) async {
        await context.perform {
            let settledPayment = SettledPayment(context: context)
            settledPayment.amount = paymentInfo.amount
            settledPayment.date = paymentInfo.paymentDate
            settledPayment.payer = paymentInfo.payer
            settledPayment.payerName = paymentInfo.payerName
            settledPayment.payee = paymentInfo.payee
            settledPayment.payeeName = paymentInfo.payeeName
            settledPayment.comment = paymentInfo.comment
            settledPayment.attachment = paymentInfo.attachment
            settledPayment.currency = paymentInfo.currency
            settledPayment.id = UUID()
            settledPayment.group = group

            try? context.save()
        }
    }
}
