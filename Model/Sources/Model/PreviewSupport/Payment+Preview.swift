//
//  File.swift
//
//
//  Created by Pascal Hintze on 08.08.2024.
//

#if DEBUG
import Foundation
import CloudKit

extension Payment {
    public static var exampleFriend: Payment {
        let payment = Payment(context: PersistenceController(inMemory: true).persistentContainer.viewContext)
//        let friend = Group.exampleFriend

        payment.id = UUID()
        payment.amount = 10.0
        payment.openAmount = 10.0
        payment.currency = "USD"
        payment.entryDate = .now
        payment.paymentCategory = 0
        payment.paymentDescription = "This is a description"
//        payment.paymentOwner = friend.id
        payment.friendAmount = 5
        payment.userAmount = 5

        return payment
    }

    public static var exampleGroup: Payment {
        let payment = Payment(context: PersistenceController(inMemory: true).persistentContainer.viewContext)
//        let group = Model.Group.exampleGroup

        payment.id = UUID()
        payment.amount = 10.0
        payment.openAmount = 10.0
        payment.currency = "USD"
        payment.entryDate = .now
        payment.paymentCategory = 0
        payment.paymentDescription = "This is a description"
        payment.groupId = "123456"
        payment.paymentOwner = "123456"
//        payment.friendAmount = 5
//        payment.userAmount = 5

        return payment
    }
}
#endif
