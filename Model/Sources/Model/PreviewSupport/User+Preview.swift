//
//  File.swift
//  
//
//  Created by Pascal Hintze on 08.08.2024.
//

#if DEBUG
import Foundation

extension User {
    public static var example: User {
        let user = User(context: PersistenceController(inMemory: true).persistentContainer.viewContext)
        user.ckID = "156156165"
        user.username = "Kobe Bryant"

        return user
    }
}
#endif
