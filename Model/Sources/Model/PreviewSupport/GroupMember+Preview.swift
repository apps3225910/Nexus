//
//  File.swift
//
//
//  Created by Pascal Hintze on 26.08.2024.
//

#if DEBUG
import Foundation
import SwiftUICore
import UIKit

extension GroupMember {
    public static var example: GroupMember {
        let groupMember = GroupMember(context: PersistenceController(inMemory: true).persistentContainer.viewContext)

        if let uiImage = UIImage(systemName: "person.circle") {
            if let imageData = uiImage.pngData() {
                groupMember.picture = imageData
            }
        }
        groupMember.id = UUID()
        groupMember.amount = 0
        groupMember.friendName = "Peter"
        groupMember.groupId = "123456"

        return groupMember
    }
}
#endif
