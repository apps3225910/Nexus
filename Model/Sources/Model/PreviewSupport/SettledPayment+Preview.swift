//
//  File.swift
//  Model
//
//  Created by Pascal Hintze on 15.11.2024.
//

#if DEBUG
import Foundation
import CloudKit
import CoreData

extension SettledPayment {
    public static var example: SettledPayment {
        let settledPayment = SettledPayment(
            context: PersistenceController(inMemory: true).persistentContainer.viewContext
        )
        settledPayment.amount = 10.0
//        settledPayment.attachment = nil
        settledPayment.comment = "This is a comment"
        settledPayment.date = .now
        settledPayment.group = Group.exampleGroup
        settledPayment.id = UUID()
        settledPayment.payee = "123456"
        settledPayment.payeeName = "Peter"
        settledPayment.payer = "654321"
        settledPayment.payerName = "Sara"
        settledPayment.currency = "USD"
        settledPayment.isSettledPaymentDeleted = false

        return settledPayment
    }
}

#endif
