//
//  LargeDataGenerator.swift
//  NexusTests
//
//  Created by Pascal Hintze on 01.02.2025.
//

import Foundation
import CoreData

extension PersistenceController {
    public func generateData(context: NSManagedObjectContext) async throws {
        try await context.perform {
            for groupCount in 1...60 {
              let group = Group(context: context)
                group.id = UUID()
                group.name = "Group \(groupCount)"
                group.openAmount = 0
                group.groupColor = .blue
                group.groupType = .home
                group.owner = "123456"
                group.isGroupDeleted = false

                for groupMemberCount in 1...4 {
                    let groupMember = GroupMember(context: context)
                    groupMember.friendName = "Friend \(groupMemberCount)"
                    groupMember.friendId = UUID().uuidString
                    groupMember.amount = 0
                    groupMember.group = group
                    groupMember.groupId = group.id.uuidString
                }

                group.owner = group.groupMember.first?.friendId ?? "123456"

                for groupPaymentCount in 1...10 {
                    let payment = Payment(context: context)
                    payment.amount = Double.random(in: 1...100)
                    payment.currency = "USD"
                    payment.entryDate = Date(timeIntervalSinceNow: -TimeInterval.random(in: 0...3600000))
                    payment.friendAmount = payment.amount / 2
                    payment.groupId = group.id.uuidString
                    payment.id = UUID()
                    payment.isPaymentDeleted = false
                    payment.openAmount = payment.amount
                    payment.paymentCategory = 1
                    payment.paymentDescription = "Payment \(groupPaymentCount)"
                    payment.paymentOwner = group.groupMember.first?.friendId ?? "123456"
                    payment.paymentOwnerName = group.groupMember.first?.friendName ?? "Unknown"
                    payment.paymentSplit = 1
                    payment.userAmount = payment.amount / 2

                    payment.group = group

                    let paymentAmountOwner = PaymentAmount(context: context)
                    paymentAmountOwner.memberId = payment.paymentOwner
                    paymentAmountOwner.amount = payment.userAmount
                    paymentAmountOwner.openAmount = payment.userAmount
                    paymentAmountOwner.payment = payment

                    let paymentAmountFriend = PaymentAmount(context: context)
                    paymentAmountFriend.memberId = group.groupMember.first(
                        where: { $0.friendId != payment.paymentOwner })?.friendId ?? "123456"
                    paymentAmountFriend.amount = payment.friendAmount
                    paymentAmountFriend.openAmount = payment.friendAmount
                    paymentAmountFriend.payment = payment
                }

                let saveCount = 5
                if 0 == (groupCount % saveCount) {
                    try context.save()
                    context.reset()
                }
            }

            if context.hasChanges {
                try context.save()
                context.reset()
            }
        }
    }

    public func deleteLargeData(context: NSManagedObjectContext) {
        let fetchRequest = Group.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Group.name, ascending: true)]

        do {
            let groups = try context.fetch(fetchRequest)

            for group in groups {
                context.delete(group)
                try context.save()
            }
        } catch {
            print("Failed to fetch and delete groups: \(error)")
        }
    }
}
