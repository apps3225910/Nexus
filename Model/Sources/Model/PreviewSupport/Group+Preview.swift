//
//  File.swift
//  
//
//  Created by Pascal Hintze on 08.08.2024.
//

#if DEBUG
import Foundation
import CloudKit
import CoreData

extension Group {
    public static var exampleGroup: Group {
        let group = Group(context: PersistenceController(inMemory: true).persistentContainer.viewContext)
        group.id = UUID()
        group.name = "Switzerland"
        group.payment = [Payment.exampleGroup, Payment.exampleGroup]
        group.openAmount = 0
        group.groupMember = [GroupMember.example, GroupMember.example]
        group.settledPayment = [SettledPayment.example, SettledPayment.example]

        return group
    }
}

#endif
