//
//  SwiftUIView.swift
//  
//
//  Created by Pascal Hintze on 03.09.2024.
//

import Foundation

@objc public enum GroupTypes: Int16, CaseIterable, Identifiable {
     case home
     case trip
     case other

    public var id: Int16 { self.rawValue }

    public var name: String {
        switch self {
        case .home:
            "Home"
        case .trip:
            "Trip"
        case .other:
            "Other"
        }
    }

    public var icon: String {
        switch self {
        case .home:
            "house"
        case .trip:
            "airplane"
        case .other:
            "list.bullet.clipboard"
        }
    }
 }
