//
//  SwiftUIView.swift
//  
//
//  Created by Pascal Hintze on 03.09.2024.
//

import SwiftUI

@objc public enum PaymentSplit: Int16, CaseIterable, Identifiable {
    case equally
    case percent
    case custom

    public var id: Int16 { self.rawValue }

    public var name: String {
        switch self {
        case .equally:
            return "Equally"
        case .percent:
            return "Percent"
        case .custom:
            return "Custom"
        }
    }
}
