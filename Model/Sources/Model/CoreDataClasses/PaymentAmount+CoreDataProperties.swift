//
//  PaymentAmount+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 02.09.2024.
//
//

import Foundation
import CoreData

extension PaymentAmount {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PaymentAmount> {
        return NSFetchRequest<PaymentAmount>(entityName: "PaymentAmount")
    }

    @NSManaged public var amount: Double
    @NSManaged public var memberId: String
    @NSManaged public var payment: Payment?
    @NSManaged public var openAmount: Double

}

extension PaymentAmount: Identifiable {

}
