//
//  User+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 30.05.2024.
//
//

import Foundation
import CoreData

extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var ckID: String
    @NSManaged public var username: String
    @NSManaged public var profilepicture: Data

}

extension User: Identifiable {

}
