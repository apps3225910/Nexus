//
//  User+CoreDataClass.swift
//  Nexus
//
//  Created by Pascal Hintze on 30.05.2024.
//
//

import Foundation
import CoreData

public class User: NSManagedObject, Codable {

        enum CodingKeys: String, CodingKey {
            case ckID
            case username
        }

        public required convenience init(from decoder: Decoder) throws {
            guard let context = decoder.userInfo[.managedObjectContext] as? NSManagedObjectContext else {
                throw DecoderConfigurationError.missingManagedObjectContext
            }

            self.init(context: context)

            do {
                let container = try decoder.container(keyedBy: CodingKeys.self)
                ckID = try container.decode(String.self, forKey: .ckID)
                username = try container.decode(String.self, forKey: .username)
            } catch DecodingError.dataCorrupted(let context) {
                print(context)
            } catch DecodingError.keyNotFound(let key, let context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.valueNotFound(let value, let context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch DecodingError.typeMismatch(let type, let context) {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }

        public func encode(to encoder: any Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(ckID, forKey: .ckID)
            try container.encode(username, forKey: .username)
        }
}

public enum DecoderConfigurationError: Error {
    case missingManagedObjectContext
}

public extension CodingUserInfoKey {
  static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")!
}
