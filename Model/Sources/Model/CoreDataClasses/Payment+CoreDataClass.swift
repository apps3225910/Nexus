//
//  Payment+CoreDataClass.swift
//  Nexus
//
//  Created by Pascal Hintze on 20.04.2024.
//
//

import Foundation
import CoreData

public class Payment: NSManagedObject {
   public var split: PaymentSplit {
        get {
            PaymentSplit(rawValue: paymentSplit) ?? .equally
        }
        set {
            paymentSplit = newValue.rawValue
        }
    }

    public var category: PaymentCategory {
         get {
             PaymentCategory(rawValue: paymentCategory) ?? .general
         }
         set {
             paymentCategory = newValue.rawValue
         }
     }
}
