//
//  SettledPayment+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 03.11.2024.
//
//

import Foundation
import CoreData

extension SettledPayment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SettledPayment> {
        return NSFetchRequest<SettledPayment>(entityName: "SettledPayment")
    }

    @NSManaged public var amount: Double
    @NSManaged public var payer: String
    @NSManaged public var payerName: String
    @NSManaged public var id: UUID
    @NSManaged public var payee: String
    @NSManaged public var payeeName: String
    @NSManaged public var date: Date
    @NSManaged public var comment: String
    @NSManaged public var attachment: Data?
    @NSManaged public var group: Group
    @NSManaged public var currency: String
    @NSManaged public var isSettledPaymentDeleted: Bool

}

extension SettledPayment: Identifiable {

}
