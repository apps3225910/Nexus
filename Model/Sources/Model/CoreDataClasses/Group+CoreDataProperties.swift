//
//  Group+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 19.07.2024.
//
//

import Foundation
import CoreData
import SwiftUI

extension Group {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Group> {
        return NSFetchRequest<Group>(entityName: "Group")
    }

    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var openAmount: Double
    @NSManaged public var type: Int16
    @NSManaged public var groupMember: Set<GroupMember>
    @NSManaged public var payment: Set<Payment>
    @NSManaged public var color: Int16
    @NSManaged public var owner: String
    @NSManaged public var settledPayment: Set<SettledPayment>
    @NSManaged public var isGroupDeleted: Bool

}

// MARK: Generated accessors for groupMember
extension Group {

    @objc(addGroupMemberObject:)
    @NSManaged public func addToGroupMember(_ value: GroupMember)

    @objc(removeGroupMemberObject:)
    @NSManaged public func removeFromGroupMember(_ value: GroupMember)

    @objc(addGroupMember:)
    @NSManaged public func addToGroupMember(_ values: NSSet)

    @objc(removeGroupMember:)
    @NSManaged public func removeFromGroupMember(_ values: NSSet)

}

// MARK: Generated accessors for payment
extension Group {

    @objc(addPaymentObject:)
    @NSManaged public func addToPayment(_ value: Payment)

    @objc(removePaymentObject:)
    @NSManaged public func removeFromPayment(_ value: Payment)

    @objc(addPayment:)
    @NSManaged public func addToPayment(_ values: NSSet)

    @objc(removePayment:)
    @NSManaged public func removeFromPayment(_ values: NSSet)

}

// MARK: Generated accessors for settledPayment
extension Group {

    @objc(addSettledPaymentObject:)
    @NSManaged public func addToSettledPayment(_ value: SettledPayment)

    @objc(removeSettledPaymentObject:)
    @NSManaged public func removeFromSettledPayment(_ value: SettledPayment)

    @objc(addSettledPayment:)
    @NSManaged public func addToSettledPayment(_ values: NSSet)

    @objc(removeSettledPayment:)
    @NSManaged public func removeFromSettledPayment(_ values: NSSet)

}

extension Group: Identifiable {

}

extension Group {
    public var groupType: GroupTypes {
        get {
            return GroupTypes(rawValue: Int16(self.type)) ?? .home
        }
        set {
            self.type = Int16(newValue.rawValue)
        }
    }
}

extension Group {
    public var groupColor: GroupColors {
        get {
            return GroupColors(rawValue: Int16(self.color)) ?? .blue
        }
        set {
            self.color = Int16(newValue.rawValue)
        }
    }
}
