//
//  Payment+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 20.04.2024.
//
//

import Foundation
import CoreData

extension Payment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Payment> {
        return NSFetchRequest<Payment>(entityName: "Payment")
    }

    @NSManaged public var amount: Double
    @NSManaged public var currency: String
    @NSManaged public var entryDate: Date
    @NSManaged public var id: UUID
    @NSManaged public var paymentDescription: String
    @NSManaged public var paymentType: Int16
    @NSManaged public var paymentOwner: String
    @NSManaged public var paymentOwnerName: String
    @NSManaged public var friendAmount: Double
    @NSManaged public var userAmount: Double
    @NSManaged public var openAmount: Double
    @NSManaged public var groupId: String
    @NSManaged public var paymentCategory: Int16
    @NSManaged public var group: Group
    @NSManaged public var paymentAmount: NSSet?
    @NSManaged public var paymentSplit: Int16
    @NSManaged public var isPaymentDeleted: Bool

    // Helper to get the amount for a specific member
    public func amountFor(memberId: String) -> Double? {
        let paymentAmountsArray = paymentAmount?.compactMap { $0 as? PaymentAmount }
        return paymentAmountsArray?.first(where: { $0.memberId == memberId })?.amount
    }

    // Update the amount for a specific member
    public func updateAmount(for memberId: String, newAmount: Double) {
        if let paymentAmount = paymentAmount?.compactMap({
            $0 as? PaymentAmount }).first(where: { $0.memberId == memberId }) {
            paymentAmount.amount = newAmount
        }
    }

    // Helper to get the percentage for a specific member
    public func percentageFor(memberId: String) -> Double? {
        guard amount > 0 else { return nil }
        if let percentAmount = paymentAmount?.compactMap({
            $0 as? PaymentAmount }).first(where: { $0.memberId == memberId })?.amount {
            return percentAmount / amount
        }
        return nil
    }

    // Helper to update the amount for a specific percentage
    public func updateAmountForPercentage(memberId: String, percentage: Double) {
        if let paymentAmount = paymentAmount?.compactMap({
            $0 as? PaymentAmount }).first(where: { $0.memberId == memberId }) {
            paymentAmount.amount = percentage * amount
        }
    }

    // Helper to calculate the total amount of all group member amounts
    public func calculateGroupMemberTotalAmount(payment: Payment, group: Model.Group) -> Double {
        var enteredAmount = 0.0
        for member in (Array(group.groupMember)) {
            enteredAmount += payment.amountFor(memberId: member.friendId) ?? 0.0
        }
        return enteredAmount
    }
}

// MARK: Generated accessors for paymentAmount
extension Payment {

    @objc(addPaymentAmountObject:)
    @NSManaged public func addToPaymentAmount(_ value: PaymentAmount)

    @objc(removePaymentAmountObject:)
    @NSManaged public func removeFromPaymentAmount(_ value: PaymentAmount)

    @objc(addPaymentAmount:)
    @NSManaged public func addToPaymentAmount(_ values: NSSet)

    @objc(removePaymentAmount:)
    @NSManaged public func removeFromPaymentAmount(_ values: NSSet)

}

extension Payment: Identifiable {

}
