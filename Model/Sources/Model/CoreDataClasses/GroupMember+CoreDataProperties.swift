//
//  GroupMember+CoreDataProperties.swift
//  Nexus
//
//  Created by Pascal Hintze on 06.08.2024.
//
//

import Foundation
import CoreData

extension GroupMember {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GroupMember> {
        return NSFetchRequest<GroupMember>(entityName: "GroupMember")
    }

    @NSManaged public var amount: Double
    @NSManaged public var friendId: String
    @NSManaged public var friendName: String
    @NSManaged public var groupId: String
    @NSManaged public var group: Group
    @NSManaged public var id: UUID
    @NSManaged public var picture: Data

}

extension GroupMember: Identifiable {

}
