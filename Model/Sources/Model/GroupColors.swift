//
//  SwiftUIView.swift
//
//
//  Created by Pascal Hintze on 03.09.2024.
//

import Foundation
import SwiftUI

@objc public enum GroupColors: Int16, CaseIterable, Identifiable {
    case red
    case blue
    case green
    case yellow
    case orange
    case purple
    case gray
    case brown

    public var id: Int16 { self.rawValue }

    public var name: String {
        switch self {
        case .red:
                "Red"
        case .blue:
                "Blue"
        case .green:
                "Green"
        case .yellow:
                "Yellow"
        case .orange:
                "Orange"
        case .purple:
                "Purple"
        case .gray:
                "Gray"
        case .brown:
                "Brown"
        }
    }

    public var colorValue: Color {
        switch self {
        case .red:
                .red
        case .blue:
                .blue
        case .green:
                .green
        case .yellow:
                .yellow
        case .orange:
                .orange
        case .purple:
                .purple
        case .gray:
                .gray
        case .brown:
                .brown
        }
    }
}
