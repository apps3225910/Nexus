//
//  File.swift
//  Model
//
//  Created by Pascal Hintze on 20.11.2024.
//

import Foundation

public struct ExpenseInfo {
    public let amount: Double
    public let currency: String
    public let date: Date
    public let category: PaymentCategory
    public let description: String
    public let paidBy: GroupMember
    public let paymentSplit: PaymentSplit
    public let group: Model.Group
    public let amounts: [String: Double]

    public init(amount: Double, currency: String, date: Date, category: PaymentCategory, description: String,
                paidBy: GroupMember, paymentSplit: PaymentSplit,
                group: Model.Group, amounts: [String: Double]) {
        self.amount = amount
        self.currency = currency
        self.date = date
        self.category = category
        self.description = description
        self.paidBy = paidBy
        self.paymentSplit = paymentSplit
        self.group = group
        self.amounts = amounts
    }
}
