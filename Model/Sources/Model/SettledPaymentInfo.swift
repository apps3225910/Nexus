//
//  Untitled.swift
//  Model
//
//  Created by Pascal Hintze on 12.11.2024.
//

import Foundation

public struct SettledPaymentInfo {
    public let amount: Double
    public let paymentDate: Date
    public let payer: String
    public let payerName: String
    public let payee: String
    public let payeeName: String
    public let comment: String
    public let attachment: Data
    public let currency: String

    public init(amount: Double, paymentDate: Date, payer: String, payerName: String, payee: String, payeeName: String,
                comment: String, attachment: Data, currency: String) {
        self.amount = amount
        self.paymentDate = paymentDate
        self.payer = payer
        self.payerName = payerName
        self.payee = payee
        self.payeeName = payeeName
        self.comment = comment
        self.attachment = attachment
        self.currency = currency
    }
}
