//
//  SwiftUIView.swift
//  
//
//  Created by Pascal Hintze on 03.09.2024.
//

import Foundation

@objc public enum PaymentOwner: Int16, CaseIterable {
    case user
    case friend

    public var name: String {
        switch self {
        case .user:
            return "You"
        case .friend:
            return "Friend"
        }
    }
}
