# Nexus

Nexus is an app which allows users to track expenses with friends and groups so they do not loose sight of open balances. The app is currently still in development. 

## Description

The app allows the user to create a payment entry and track all their payments with a friend or in a group. Payments can be created in different currencies to support multiple countries.

## Features
- Create and track payments with friends and groups
- Split your payment amounts by percent or a custom value
- See your open balances
- Supports different currencies

## Upcoming Features
- Add notifications when a new payment was created
- Exchange API to calculate the exchange rates for different currencies
- Add photos/attachments of bills to payments
- Implement a history chart to see all spendings for different time periods 

## Reason for development
The main goal of this app is to get a good understanding of Core Data together with CloudKit. 

## Screenshots

Screenshots to come

**Sign In**

<p float="left">
![alt text](Screenshots/Nexus_SignIn.png "Title Text"){width=234 height=510px}
</p>

**Friends**

tbd


**Groups**
<p float="left">
![alt text](Screenshots/Nexus_Groups_Empty.png "Title Text"){width=234 height=510px}
&nbsp; &nbsp; &nbsp; &nbsp;
![alt text](Screenshots/Nexus_GroupList.png "Title Text"){width=234 height=510px}x}
</p>

Create Group:
<p float="left">
![alt text](Screenshots/Nexus_CreateGroup.png "Title Text"){width=234 height=510px}
</p>

Group Details:
<p float="left">
![alt text](Screenshots/Nexus_GroupExpenses.png "Title Text"){width=234 height=510px}
&nbsp; &nbsp; &nbsp; &nbsp;
![alt text](Screenshots/Nexus_GroupBalances.png "Title Text"){width=234 height=510px}
</p>

Group Payment:
<p float="left">
![alt text](Screenshots/Nexus_AddGroupPayment.png "Title Text"){width=234 height=510px}
</p>

**Activities**

tbd


**Account**
<p float="left">
![alt text](Screenshots/Nexus_Profile.png "Title Text"){width=234 height=510px}