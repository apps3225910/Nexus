//
//  NexusUITests.swift
//  NexusUITests
//
//  Created by Pascal Hintze on 02.02.2025.
//

import XCTest

final class PaymentUITests: XCTestCase {
    private var app: XCUIApplication!

    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()

        // Delete all groups
        app.navigationBars["Groups"].staticTexts["Data generator"].tap()
        app.buttons["Delete Data"].tap()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    @MainActor
    func testExample() throws {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
}
