//
//  Group+UITests.swift
//  NexusUITests
//
//  Created by Pascal Hintze on 02.02.2025.
//

import XCTest

final class GroupUITests: XCTestCase {
    private var app: XCUIApplication!

    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()

        // Delete all groups
        app.navigationBars["Groups"].staticTexts["Data generator"].tap()
        app.buttons["Delete Data"].tap()

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCreateGroup() throws {
        app.navigationBars["Groups"].buttons["Add Group"].tap()

        XCTAssert(app.navigationBars["New Group"].exists, "Could not navigate to new group view")

        let collectionViewsQuery = app.collectionViews

        collectionViewsQuery.textFields["Group Name"].tap()
        app.typeText("Test Group 1")
        collectionViewsQuery.staticTexts["Home"].tap()
        collectionViewsQuery.buttons["airplane"].tap()
        collectionViewsQuery.staticTexts["Blue"].tap()
        collectionViewsQuery.buttons["Red"].tap()
        app.navigationBars["New Group"].buttons["Save"].tap()

        XCTAssertEqual(1, app.collectionViews["FilteredGroupList"].cells.count, "The list contains no new group" )
    }

    func testDeleteGroupInList() throws {
        app.navigationBars["Groups"].buttons["Add Group"].tap()

        XCTAssert(app.navigationBars["New Group"].exists, "Could not navigate to new group view")

        let collectionViewsQuery = app.collectionViews

        collectionViewsQuery.textFields["Group Name"].tap()
        app.typeText("Test Group 1")
        collectionViewsQuery.staticTexts["Home"].tap()
        collectionViewsQuery.buttons["airplane"].tap()
        collectionViewsQuery.staticTexts["Blue"].tap()
        collectionViewsQuery.buttons["Red"].tap()
        app.navigationBars["New Group"].buttons["Save"].tap()

        XCTAssertEqual(1, app.collectionViews["FilteredGroupList"].cells.count, "The list contains no new group" )

        app.collectionViews["FilteredGroupList"].cells.firstMatch.swipeLeft()
        app.buttons["Delete"].firstMatch.tap()

        XCTAssertEqual(0, app.collectionViews["FilteredGroupList"].cells.count, "The list contains a group" )
    }

    func testDeleteGroup() throws {
        app.navigationBars["Groups"].buttons["Add Group"].tap()

        XCTAssert(app.navigationBars["New Group"].exists, "Could not navigate to new group view")

        let collectionViewsQuery = app.collectionViews

        collectionViewsQuery.textFields["Group Name"].tap()
        app.typeText("Test Group 1")
        collectionViewsQuery.staticTexts["Home"].tap()
        collectionViewsQuery.buttons["airplane"].tap()
        collectionViewsQuery.staticTexts["Blue"].tap()
        collectionViewsQuery.buttons["Red"].tap()
        app.navigationBars["New Group"].buttons["Save"].tap()

        XCTAssertEqual(1, app.collectionViews["FilteredGroupList"].cells.count, "The list contains no new group" )

        app.collectionViews["FilteredGroupList"].cells.firstMatch.tap()
        app.buttons["Edit"].tap()
        app.buttons["Delete Group"].tap()

        XCTAssertEqual(0, app.collectionViews["FilteredGroupList"].cells.count, "The list contains a group" )
    }

    func testGroupListScrolling() throws {
        app.navigationBars["Groups"].staticTexts["Data generator"].tap()
        app.buttons["Generate Data"].tap()

        measure(metrics: [XCTOSSignpostMetric.scrollingAndDecelerationMetric]) {
            app.swipeUp()
            app.swipeDown()
        }
    }

    func testGroupListFiltering() throws {
        app.navigationBars["Groups"].staticTexts["Data generator"].tap()
        app.buttons["Generate Data"].tap()

        app.searchFields["Groups"].tap()
        app.typeText("Group 10")

        XCTAssertEqual(1, app.collectionViews["FilteredGroupList"].cells.count, "The list does not contain group 10")

        app.collectionViews["FilteredGroupList"].cells.firstMatch.tap()
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
