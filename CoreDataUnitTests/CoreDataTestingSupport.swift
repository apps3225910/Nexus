//
//  CoreDataTestingSupport.swift
//  NexusTests
//
//  Created by Pascal Hintze on 01.02.2025.
//

import Foundation
import Model
import Testing
import CoreData

class CoreDataTestingSupport {
    // From CoreDataCloudKitDemo
    public func resetCoreData(for persistenceController: PersistenceController) {
        let model = persistenceController.persistentContainer.managedObjectModel
        let context = persistenceController.persistentContainer.viewContext
        context.performAndWait {
            for entityName in model.entitiesByName.keys {
                do {
                    let request = NSBatchDeleteRequest(fetchRequest: NSFetchRequest(entityName: entityName))
                    request.resultType = .resultTypeStatusOnly
                    guard let deleteResult = try context.execute(request) as? NSBatchDeleteResult else {
                        Issue.record("Unexpected result from batch delete for \(entityName)")
                        return
                    }

                    guard let status = deleteResult.result as? NSNumber else {
                        Issue.record("Expected an \(NSNumber.self) from batch delete for \(entityName)")
                        return
                    }
                    #expect(status.boolValue)
                } catch let error {
                    Issue.record("Failed to batch delete data from test run for entity \(entityName): \(error)")
                }
            }
            context.reset()
        }
    }
}
