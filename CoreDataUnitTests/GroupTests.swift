//
//  NexusTests.swift
//  NexusTests
//
//  Created by Pascal Hintze on 27.01.2025.
//

import Testing
@testable import Model
import CoreData

@Suite("Group Tests") class GroupTests {
    var persistenceController: PersistenceController!

    var context: NSManagedObjectContext {
        return persistenceController.persistentContainer.viewContext
    }

    init() {
        persistenceController = PersistenceController(inMemory: true)
    }

    deinit {
        CoreDataTestingSupport().resetCoreData(for: persistenceController)
    }

    @Test("Create Group")
    func groupCreation() async throws {
        var newGroupMember: GroupMember?
        var countBefore = 0

        await context.perform {
            let fetchBefore = Group.fetchRequest()
            countBefore = (try? self.context.fetch(fetchBefore).count) ?? 0

            let groupMember = GroupMember(context: self.context)
            groupMember.friendName = "Test Friend"
            groupMember.friendId = "123456"
            groupMember.amount = 0
            groupMember.picture = Data()

            newGroupMember = groupMember
        }
        await persistenceController.addGroup(name: "Test Group 1", type: .home, owner: newGroupMember!,
                                             groupColor: .blue, context: self.context)

        await context.perform {
            let fetchAfter = Group.fetchRequest()
            let countAfter = (try? self.context.fetch(fetchAfter).count) ?? 0

            #expect(countAfter == countBefore + 1)
        }
    }

    @Test("Create Additional GroupMember")
    func additionalGroupMemberCreation() async throws {
        var newGroupMember: GroupMember?

        await context.perform {
            let groupMember = GroupMember(context: self.context)
            groupMember.friendName = "Test Friend"
            groupMember.friendId = "123456"
            groupMember.amount = 0
            groupMember.picture = Data()

            newGroupMember = groupMember
        }
        await persistenceController.addGroup(name: "Test Group 2", type: .home, owner: newGroupMember!,
                                             groupColor: .blue, context: context)

        await context.perform {
            let fetchRequest = Group.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name == %@", "Test Group 2")

            guard let group = try? self.context.fetch(fetchRequest).first else {
                print("Failed to fetch group")
                return
            }

            let initialMemberCount = group.groupMember.count

            let additionalGroupMember = GroupMember(context: self.context)
            additionalGroupMember.friendName = "Additional Friend"
            additionalGroupMember.friendId = "654321"
            additionalGroupMember.amount = 0
            additionalGroupMember.picture = Data()
            additionalGroupMember.group = group

            #expect(group.groupMember.count == initialMemberCount + 1)
        }
    }
}
