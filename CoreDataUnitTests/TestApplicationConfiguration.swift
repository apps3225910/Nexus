////
////  TestApplicationConfiguration.swift
////  NexusTests
////
////  Created by Pascal Hintze on 01.02.2025.
////
//
// import Testing
// @testable import Nexus
// import Foundation
//
// struct TestApplicationConfiguration {
//    @Test() func testCoreDataStackObeysLaunchArguments() {
//        let appDelegate = AppDelegate.sharedAppDelegate
//        #expect(appDelegate.testingEnabled,
//                "Should have launched with testing enabled so the test cases don't use the customer store.")
//        #expect(!appDelegate.allowCloudKitSync,
//                "Should have launched with CloudKit disabled so cloud sync doesn't impact the test dataset.")
//        let container = appDelegate.persistenceController.persistentContainer
//        for storeDescription in container.persistentStoreDescriptions {
//            #expect(storeDescription.cloudKitContainerOptions == nil,
//                    "Shouldn't be using CloudKit during unit / UI tests.")
//        }
//    }
//
//    @Test func testInfoPlistLocationMessage() throws {
//        for key in [ "NSLocationUsageDescription", // for macOS
//                     "NSLocationWhenInUseUsageDescription" ] {
//            if let message = Bundle(for: AppDelegate.self).infoDictionary?[key] as? String {
//                #expect("Location data is associated with each Post when it is created for later reference." == message)
//            } else {
//                Issue.record("The app bundle appears to missing info plist entry for \(key)")
//            }
//        }
//    }
//
// }
