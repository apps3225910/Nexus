//
//  TestCoreDataStack.swift
//  NexusTests
//
//  Created by Pascal Hintze on 01.02.2025.
//

import Testing
@testable import Model
import CoreData

@Suite("TestCoreDataStack") class TestCoreDataStack {
    var persistenceController: PersistenceController!

    init() {
        persistenceController = PersistenceController(inMemory: true)
    }

    deinit {
        CoreDataTestingSupport().resetCoreData(for: persistenceController)
    }

    @Test func testStoreAssignments() {
        #expect(persistenceController.privatePersistentStore != nil)
        #expect("private.sqlite" == persistenceController.privatePersistentStore.url?.lastPathComponent)

        #expect(persistenceController.sharedPersistentStore != nil)
        #expect("shared.sqlite" == persistenceController.sharedPersistentStore.url?.lastPathComponent)

        #expect(persistenceController.publicPersistentStore != nil)
        #expect("public.sqlite" == persistenceController.publicPersistentStore.url?.lastPathComponent)
    }

    @Test func testModel() throws {
        for (entityName, entity) in persistenceController.persistentContainer.managedObjectModel.entitiesByName {
            switch entityName {
            case "Group": verifyGroup(entity)
            case "GroupMember": verifyGroupMember(entity)
            case "Payment": verifyPayment(entity)
            case "PaymentAmount": verifyPaymentAmount(entity)
            case "SettledPayment": verifySettledPayment(entity)
            case "User": verifyUser(entity)
            default: Issue.record("I don't know how to validate \(entityName), is it new?")
            }
        }
    }

   @Test func testCoreDataStackRegistersForRemoteChangeNotifications() throws {
       let originalContainer = persistenceController.persistentContainer
        let alternateContainer = NSPersistentContainer(name: "alternateContainer",
                                                       managedObjectModel: originalContainer.managedObjectModel)

        alternateContainer.persistentStoreDescriptions = originalContainer.persistentStoreDescriptions
        alternateContainer.loadPersistentStores(completionHandler: { (_, error) in
            if let loadError = error as NSError? {
                fatalError("###\(#function): Failed to load persistent stores:\(loadError)")
            }
        })

        let alternateContext = alternateContainer.viewContext
        alternateContext.transactionAuthor = "AlternateContextAuthor"
        alternateContext.performAndWait {
            let group = NSEntityDescription.insertNewObject(forEntityName: "Group",
                                                           into: alternateContext)
            group.setValue("Such name", forKey: "name")
            group.setValue("Very owner.", forKey: "owner")

            do {
                try alternateContext.save()
            } catch let error {
                Issue.record("Save failed: \(error)")
            }
        }

        RunLoop.current.run(until: Date(timeIntervalSinceNow: 1))

        let context = originalContainer.viewContext
        context.performAndWait {
            let fetchRequest: NSFetchRequest<Group> = Group.fetchRequest()
            let groups = try? context.fetch(fetchRequest)
            let numPosts = UInt32(groups?.count ?? 0)

            #expect(numPosts == 1)
        }
    }

    // MARK: Model Validation
    func verifyGroup(_ entity: NSEntityDescription) {
        verifyAttribute(named: "color",
                        on: entity,
                        type: .integer16)
        verifyAttribute(named: "id",
                        on: entity,
                        type: .uuid)
        verifyAttribute(named: "isGroupDeleted",
                        on: entity,
                        type: .boolean)
        verifyAttribute(named: "name",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "openAmount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "owner",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "type",
                        on: entity,
                        type: .integer16)

        verifyRelationship(named: "groupMember",
                           on: entity,
                           destination: "GroupMember",
                           inverse: "group",
                           isToMany: true)
        verifyRelationship(named: "payment",
                           on: entity,
                           destination: "Payment",
                           inverse: "group",
                           isToMany: true)
        verifyRelationship(named: "settledPayment",
                           on: entity,
                           destination: "SettledPayment",
                           inverse: "group",
                           isToMany: true)
    }

    func verifyGroupMember(_ entity: NSEntityDescription) {
        verifyAttribute(named: "amount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "friendId",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "friendName",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "groupId",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "id",
                        on: entity,
                        type: .uuid)
        verifyAttribute(named: "picture",
                        on: entity,
                        type: .binaryData)

        verifyRelationship(named: "group",
                           on: entity,
                           destination: "Group",
                           inverse: "groupMember",
                           isToMany: false)
    }

    func verifyPayment(_ entity: NSEntityDescription) {
        verifyAttribute(named: "amount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "currency",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "entryDate",
                        on: entity,
                        type: .date)
        verifyAttribute(named: "friendAmount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "groupId",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "id",
                        on: entity,
                        type: .uuid)
        verifyAttribute(named: "isPaymentDeleted",
                        on: entity,
                        type: .boolean)
        verifyAttribute(named: "openAmount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "paymentCategory",
                        on: entity,
                        type: .integer16)
        verifyAttribute(named: "paymentDescription",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "paymentOwner",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "paymentOwnerName",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "paymentSplit",
                        on: entity,
                        type: .integer16)
        verifyAttribute(named: "userAmount",
                        on: entity,
                        type: .double)

        verifyRelationship(named: "group",
                           on: entity,
                           destination: "Group",
                           inverse: "payment",
                           isToMany: false)
        verifyRelationship(named: "paymentAmount",
                           on: entity,
                           destination: "PaymentAmount",
                           inverse: "payment",
                           isToMany: true)
    }

    func verifyPaymentAmount(_ entity: NSEntityDescription) {
        verifyAttribute(named: "amount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "memberId",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "openAmount",
                        on: entity,
                        type: .double)

        verifyRelationship(named: "payment",
                           on: entity,
                           destination: "Payment",
                           inverse: "paymentAmount",
                           isToMany: false)
    }

    func verifySettledPayment(_ entity: NSEntityDescription) {
        verifyAttribute(named: "amount",
                        on: entity,
                        type: .double)
        verifyAttribute(named: "attachment",
                        on: entity,
                        type: .binaryData)
        verifyAttribute(named: "comment",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "currency",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "date",
                        on: entity,
                        type: .date)
        verifyAttribute(named: "id",
                        on: entity,
                        type: .uuid)
        verifyAttribute(named: "isSettledPaymentDeleted",
                        on: entity,
                        type: .boolean)
        verifyAttribute(named: "payee",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "payeeName",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "payer",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "payerName",
                        on: entity,
                        type: .string)

        verifyRelationship(named: "group",
                           on: entity,
                           destination: "Group",
                           inverse: "settledPayment",
                           isToMany: false)
    }

    func verifyUser(_ entity: NSEntityDescription) {
        verifyAttribute(named: "ckID",
                        on: entity,
                        type: .string)
        verifyAttribute(named: "profilepicture",
                        on: entity,
                        type: .binaryData)
        verifyAttribute(named: "username",
                        on: entity,
                        type: .string)
    }

    func verifyAttribute(named name: String,
                         on entity: NSEntityDescription,
                         type: NSAttributeDescription.AttributeType,
                         allowsEncryption: Bool? = false,
                         valueTransformerName: NSValueTransformerName? = nil,
                         valueClassName: String? = nil) {
        guard let attribute = entity.attributesByName[name] else {
            Issue.record("\(entity.name!) is missing expected attribute \(name)")
            return
        }

        #expect(type == attribute.type)
        #expect(allowsEncryption == attribute.allowsCloudEncryption)
        if .transformable == attribute.type, let expectedValueTransformerName = valueTransformerName {
            #expect(expectedValueTransformerName.rawValue == attribute.valueTransformerName)
            if let expectedValueClassName = valueClassName {
                #expect(expectedValueClassName == attribute.attributeValueClassName)
            } else {
                #expect(attribute.attributeValueClassName == nil)
            }
        }
    }

    func verifyRelationship(named name: String,
                            on entity: NSEntityDescription,
                            destination destinationEntityName: String,
                            inverse inverseRelationshipName: String,
                            isToMany: Bool? = false) {
        guard let relationship = entity.relationshipsByName[name] else {
            Issue.record("\(entity.name!) is missing expected relationship \(name)")
            return
        }

        guard let destinationEntity = relationship.destinationEntity else {
            Issue.record(
                "Relationship \(entity.name!).\(name) is missing expected destination entity \(destinationEntityName)")
            return
        }

        #expect(destinationEntityName == destinationEntity.name)
        #expect(inverseRelationshipName == relationship.inverseRelationship?.name)
        #expect(destinationEntity.relationshipsByName[inverseRelationshipName] != nil)
        #expect(isToMany == relationship.isToMany)
    }
}
